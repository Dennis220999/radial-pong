#!/usr/bin/env python3

import pytest
import socket
import time

# imports from server
from server.network.network_handler import ServerNetworkHandler

# imports from client
from client.network.network_handler import ClientNetworkHandler
from client.network.network_handler import NetworkConfiguration

from tests.conftest_serialization import DummySerializer


STARTUP_TIME = 0.5 # time to wait or startup
SENDING_TIME = 0.01 # time to wait for message receiving


class ServerClientBundle():
    port = 8181
    def __init__(self,  number_of_clients : int):
        self.__ip = "127.0.0.1"
        
        self.__clients = []
        self.__server = ServerNetworkHandler(DummySerializer())
        self.__create_clients(number_of_clients)

    def __create_clients(self, number_of_clients):
        while number_of_clients > 0:
            # the following initilization may raise a ConnectionRefusedError if 
            # the client could not connect to the server, however such problems 
            # should also be detected in the client specific test in file
            # tests/client/test_network_communication.py
            self.__clients.append(
                ClientNetworkHandler(DummySerializer(), 
                    NetworkConfiguration(self.__server.ip_address, 
                        self.__server.port)))
            number_of_clients -= 1
    
    def getServer(self):
        while not self.__server.socket_active():
            time.sleep(STARTUP_TIME)
        return self.__server

    def close(self):
        """Close the client sockets, server socket and join all threads.
        """
        # close connections from client side
        for client in self.__clients:
            client.close_connection()
        
        # close connections from server side and join threads
        self.__server.close_connection()

    def getClient(self, player_idx : int):
        while player_idx not in self.__server._ServerNetworkHandler__used_ids:
            time.sleep(STARTUP_TIME)
        
        return self.__clients[player_idx]

@pytest.fixture(scope="function")
def free_port_localhost():
    """Returns a number of free port on localhost.

    Returns:
        int: port number of free port on localhost
    
    Raises:
        ValueError: no free port in given port range
    """
    port_range = range(8181, 8381)
    for port in port_range:
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
            # test if port is blocked
            if sock.connect_ex(("127.0.0.1", port)):
                return port    
    raise ValueError("No free port on localhost in range from %d to %d." % 
        (port_range.start, port_range.start + len(port_range)-1) )

@pytest.fixture(scope="function")
def second_free_port_localhost():
    """Returns another number of free port on localhost. This is the same as 
    free_port_localhost, but it's neccesarry due to pytest's caching of fixtures 
    per test session.

    Returns:
        int: port number of free port on localhost
    
    Raises:
        ValueError: no free port in given port range
    """
    port_range = range(8182, 8381)
    for port in port_range:
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
            # test if port is blocked
            if sock.connect_ex(("127.0.0.1", port)):
                return port    
    raise ValueError("No free port on localhost in range from %d to %d." % 
        (port_range.start, port_range.start + len(port_range)-1) )

@pytest.fixture(scope="function")
def getServerAndClients(number_of_clients: int):
    bundle = ServerClientBundle(number_of_clients)
    return bundle


@pytest.fixture(scope="function")
def free_port_localhost():
    """Returns a number of free port on localhost.

    Returns:
        int: port number of free port on localhost
    
    Raises:
        ValueError: no free port in given port range
    """
    port_range = range(8181, 8381)
    for port in port_range:
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
            # test if port is blocked
            if sock.connect_ex(("127.0.0.1", port)):
                return port    
    raise ValueError("No free port on localhost in range from %d to %d." % 
        (port_range.start, port_range.start + len(port_range)-1) )

@pytest.fixture(scope="function")
def second_free_port_localhost():
    """Returns another number of free port on localhost. This is the same as 
    free_port_localhost, but it's neccesarry due to pytest's caching of fixtures 
    per test session.

    Returns:
        int: port number of free port on localhost
    
    Raises:
        ValueError: no free port in given port range
    """
    port_range = range(8182, 8381)
    for port in port_range:
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
            # test if port is blocked
            if sock.connect_ex(("127.0.0.1", port)):
                return port    
    raise ValueError("No free port on localhost in range from %d to %d." % 
        (port_range.start, port_range.start + len(port_range)-1) )

@pytest.fixture(scope="function")
def getServerAndClients(number_of_clients: int):
    bundle = ServerClientBundle(number_of_clients)
    return bundle

@pytest.fixture(params=[0, 255, 1023], scope="function")
def example_message(request):
    return request.param

@pytest.fixture(params=[7, 253], scope="function")
def example_message2(request):
    return request.param

@pytest.fixture(scope="function")
def long_message():
    return 50*[0]

@pytest.fixture(scope="function")
def single_message():
    return 253
