#!/usr/bin/env python3

"""Script contains tests for the game_components script"""

from shared.game.game_components import Player, PlayerPlatform

class TestPlayer():
    """Class testing class Player."""

    def test_is_alive(self):
        platform = PlayerPlatform(0.5, 1., 1.)
        player = Player(platform, 42)
        assert player.is_alive()
