#!/usr/bin/env python3

import pytest

from shared.serialization.connection_confirmation_message import ConnectionConfirmationMessage, ConnectionStatus


class TestConnectionConfirmationMessage():
    """Tests class \"ConnectionConfirmationMessage\"."""
    
    def test_init(self):
        message_type_id = 2
        player_id = 0
        connection_status = ConnectionStatus.SUCCESSFUL_RUNNING
        connection_confirmation_message = ConnectionConfirmationMessage(player_id, connection_status)
        assert connection_confirmation_message.player_id == player_id
        assert connection_confirmation_message.connection_status == connection_status
        assert connection_confirmation_message.MESSAGE_TYPE_ID == message_type_id
        
        player_id = 255
        connection_confirmation_message = ConnectionConfirmationMessage(player_id, connection_status)
        assert connection_confirmation_message.player_id == player_id
        
        player_id = 256
        with pytest.raises(ValueError) as excinfo:
            connection_confirmation_message = ConnectionConfirmationMessage(player_id, connection_status)
        assert "currently: 256" in str(excinfo.value)
        
        player_id = -1
        with pytest.raises(ValueError) as excinfo:
            connection_confirmation_message = ConnectionConfirmationMessage(player_id, connection_status)
        assert "currently: -1" in str(excinfo.value)
            
    def test_serialize(self):
        player_id = 0
        connection_status = ConnectionStatus.FAILED
        connection_confirmation_message = ConnectionConfirmationMessage(player_id, connection_status)
        serialized_data = connection_confirmation_message.serialize()
        assert serialized_data == b'\x00\x00'
        
        player_id = 255
        connection_status = ConnectionStatus.SUCCESSFUL_WAITING
        connection_confirmation_message = ConnectionConfirmationMessage(player_id, connection_status)
        serialized_data = connection_confirmation_message.serialize()
        assert serialized_data == b'\x01\xFF'
        
    def test_deserialize(self):
        serialized_data = b'\x00\x00'
        connection_confirmation_message = ConnectionConfirmationMessage.deserialize(serialized_data)
        assert connection_confirmation_message.player_id == 0
        assert connection_confirmation_message.connection_status == ConnectionStatus.FAILED
        
        serialized_data = b'\x01\xFF'
        connection_confirmation_message = ConnectionConfirmationMessage.deserialize(serialized_data)
        assert connection_confirmation_message.player_id == 255
        assert connection_confirmation_message.connection_status == ConnectionStatus.SUCCESSFUL_WAITING
        
        serialized_data = b'\x00'
        with pytest.raises(ValueError) as excinfo:
            connection_confirmation_message = ConnectionConfirmationMessage.deserialize(serialized_data)
        assert "currently: 1" in str(excinfo.value)
        
        serialized_data = b'\x00\x00\x00'
        with pytest.raises(ValueError) as excinfo:
            connection_confirmation_message = ConnectionConfirmationMessage.deserialize(serialized_data)
        assert "currently: 3" in str(excinfo.value)
        
        serialized_data = b'\xFF\x00'
        with pytest.raises(ValueError) as excinfo:
            connection_confirmation_message = ConnectionConfirmationMessage.deserialize(serialized_data)
        assert "currently: 255" in str(excinfo.value)