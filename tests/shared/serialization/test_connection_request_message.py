#!/usr/bin/env python3

import pytest

from shared.serialization.connection_request_message import ConnectionRequestMessage


class TestConnectionRequestMessage():
    """Tests class \"ConnectionRequestMessage\"."""
    
    def test_init(self):
        message_type_id = 1
        player_id = 0
        connection_request_message = ConnectionRequestMessage(player_id)
        assert connection_request_message.MESSAGE_TYPE_ID == message_type_id
        assert connection_request_message.player_id == player_id
        
        player_id = -1
        with pytest.raises(ValueError) as excinfo:
            connection_request_message = ConnectionRequestMessage(player_id)
            assert "currently: -1" in str(excinfo.value)
            
        player_id = 256
        with pytest.raises(ValueError) as excinfo:
            connection_request_message = ConnectionRequestMessage(player_id)
            assert "currently: 256" in str(excinfo.value)
            
    def test_serialize(self):
        connection_request_message = ConnectionRequestMessage()
        serialized_data = connection_request_message.serialize()
        assert serialized_data == b''
        
    def test_deserialize(self):
        serialized_data = b''
        connection_request_message = ConnectionRequestMessage.deserialize(serialized_data, 0)
        assert connection_request_message.player_id == 0
        
        with pytest.raises(TypeError) as excinfo: 
            connection_request_message = ConnectionRequestMessage.deserialize(serialized_data)
        assert "not be None" in str(excinfo.value)
        
        serialized_data = b'\x00'
        with pytest.raises(ValueError) as excinfo:
            connection_request_message = ConnectionRequestMessage.deserialize(serialized_data, 0)
        assert "currently: 1" in str(excinfo.value)