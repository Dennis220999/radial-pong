#!/usr/bin/env python3

import pytest

from shared.serialization.game_stop_message import GameStopMessage


class TestGameStopMessage():
    """Tests class \"GameStopMessage\"."""
    
    def test_init(self):
        message_type_id = 5
        winner_ids = [0]
        winner_lives = 1
        game_stop_message = GameStopMessage(winner_ids, winner_lives)
        assert game_stop_message.MESSAGE_TYPE_ID == message_type_id
        assert game_stop_message.winner_ids == winner_ids
        assert game_stop_message.winner_lives == winner_lives
        
        winner_ids = []
        winner_lives = None
        game_stop_message = GameStopMessage(winner_ids, winner_lives)
        
        winner_ids = list(range(2**8 - 1))
        winner_lives = 1
        game_stop_message = GameStopMessage(winner_ids, winner_lives)
        
        winner_ids = list(range(2**8))
        with pytest.raises(ValueError) as excinfo:
            game_stop_message = GameStopMessage(winner_ids, winner_lives)
        assert "currently: 256" in str(excinfo.value)
        
        winner_ids = [-1]*(2**8 - 1)
        with pytest.raises(ValueError) as excinfo:
            game_stop_message = GameStopMessage(winner_ids, winner_lives)
        assert "currently: -1" in str(excinfo.value)
        
        winner_ids = [256]*(2**8 - 1)
        with pytest.raises(ValueError) as excinfo:
            game_stop_message = GameStopMessage(winner_ids, winner_lives)
        assert "currently: 256" in str(excinfo.value)
        
        winner_ids = list(range(2**8 - 1))
        winner_lives = 0
        with pytest.raises(ValueError) as excinfo:
            game_stop_message = GameStopMessage(winner_ids, winner_lives)
        assert "currently: 0" in str(excinfo.value)
        
        winner_lives = 256
        with pytest.raises(ValueError) as excinfo:
            game_stop_message = GameStopMessage(winner_ids, winner_lives)
        assert "currently: 256" in str(excinfo.value)
            
    def test_serialize(self):
        winner_ids = [0]
        winner_lives = 1
        game_stop_message = GameStopMessage(winner_ids, winner_lives)
        
        serialized_data = game_stop_message.serialize()
        assert serialized_data == b'\x01\x00\x01'
        
        winner_ids = []
        winner_lives = None
        game_stop_message = GameStopMessage(winner_ids, winner_lives)
        
        serialized_data = game_stop_message.serialize()
        assert serialized_data == b'\x00'
        
        winner_ids = [0]
        winner_lives = None
        game_stop_message = GameStopMessage(winner_ids, winner_lives)
        with pytest.raises(ValueError) as excinfo:
            serialized_data = game_stop_message.serialize()
        assert "currently: 1" in str(excinfo.value)
        assert "currently: False" in str(excinfo.value)
        
        winner_ids = []
        winner_lives = 1
        game_stop_message = GameStopMessage(winner_ids, winner_lives)
        with pytest.raises(ValueError) as excinfo:
            serialized_data = game_stop_message.serialize()
        assert "currently: 0" in str(excinfo.value)
        assert "currently: True" in str(excinfo.value)
        
    def test_deserialize(self):
        serialized_data = b'\x01\x00\x01'
        game_stop_message = GameStopMessage.deserialize(serialized_data)
        assert game_stop_message.winner_ids == [0]
        assert game_stop_message.winner_lives == 1
        
        serialized_data = b'\x00'
        game_stop_message = GameStopMessage.deserialize(serialized_data)
        assert game_stop_message.winner_ids == []
        assert game_stop_message.winner_lives is None
        
        serialized_data = b''
        with pytest.raises(ValueError) as excinfo:
            game_stop_message = GameStopMessage.deserialize(serialized_data)
        assert "currently: 0" in str(excinfo.value)
        
        serialized_data = 258*b'\x00'
        with pytest.raises(ValueError) as excinfo:
            game_stop_message = GameStopMessage.deserialize(serialized_data)
        assert "currently: 258" in str(excinfo.value)
        
        serialized_data = b'\x01'
        with pytest.raises(ValueError) as excinfo:
            game_stop_message = GameStopMessage.deserialize(serialized_data)
        assert "currently: 2" in str(excinfo.value)
        assert "currently: 0" in str(excinfo.value)
        
        serialized_data = b'\x00\x00\x01'
        with pytest.raises(ValueError) as excinfo:
            game_stop_message = GameStopMessage.deserialize(serialized_data)
        assert "currently: 0" in str(excinfo.value)
        assert "currently: 2" in str(excinfo.value)
        
        serialized_data = b'\x01\x00\x01\x01\x01'
        with pytest.raises(ValueError) as excinfo:
            game_stop_message = GameStopMessage.deserialize(serialized_data)
        assert "currently: 2" in str(excinfo.value)
        assert "currently: 4" in str(excinfo.value)