#!/usr/bin/env python3

import pytest

from shared.serialization.player_count_message import PlayerCountMessage


class TestPlayerCountMessage():
    """Tests class \"PlayerCountMessage\"."""
    
    def test_init(self):
        message_type_id = 9
        player_count = 1
        player_count_message = PlayerCountMessage(player_count)
        assert player_count_message.MESSAGE_TYPE_ID == message_type_id
        assert player_count_message.player_count == player_count
        
        player_count = 0
        with pytest.raises(ValueError) as excinfo:
            player_count_message = PlayerCountMessage(player_count)
        assert "currently: 0" in str(excinfo.value)
            
        player_count = 256
        with pytest.raises(ValueError) as excinfo:
            player_count_message = PlayerCountMessage(player_count)
        assert "currently: 256" in str(excinfo.value)
        
    def test_serialize(self):
        player_count = 1
        player_count_message = PlayerCountMessage(player_count)
        serialized_data = player_count_message.serialize()
        assert serialized_data == b'\x01'
        
        player_count = 255
        player_count_message = PlayerCountMessage(player_count)
        serialized_data = player_count_message.serialize()
        assert serialized_data == b'\xFF'
        
    def test_deserialize(self):
        serialized_data = b'\x01'
        player_count = 1
        player_count_message = PlayerCountMessage.deserialize(serialized_data)
        assert player_count_message.player_count == player_count
        
        serialized_data = b'\xFF'
        player_count = 255
        player_count_message = PlayerCountMessage.deserialize(serialized_data)
        assert player_count_message.player_count == player_count
        
        serialized_data = b''
        with pytest.raises(ValueError) as excinfo:
            player_count_message = PlayerCountMessage.deserialize(serialized_data)
        assert "currently: 0" in str(excinfo.value)
        
        serialized_data = b'\x00\x01'
        with pytest.raises(ValueError) as excinfo:
            player_count_message = PlayerCountMessage.deserialize(serialized_data)
        assert "currently: 2" in str(excinfo.value)
        
        serialized_data = b'\x00'
        with pytest.raises(ValueError) as excinfo:
            player_count_message = PlayerCountMessage.deserialize(serialized_data)
        assert "currently: 0" in str(excinfo.value)