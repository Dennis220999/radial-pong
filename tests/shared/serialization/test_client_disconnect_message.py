#!/usr/bin/env python3

import pytest

from shared.serialization.client_disconnect_message import ClientDisconnectMessage


class TestClientDisconnectMessage():
    """Tests class \"ClientDisconnectMessage\"."""
    
    def test_init(self):
        message_type_id = 8
        player_id = 0
        client_disconnect_message = ClientDisconnectMessage(player_id)
        assert client_disconnect_message.MESSAGE_TYPE_ID == message_type_id
        assert client_disconnect_message.player_id == player_id
        
        player_id = -1
        with pytest.raises(ValueError) as excinfo:
            client_disconnect_message = ClientDisconnectMessage(player_id)
            assert "currently: -1" in str(excinfo.value)
            
        player_id = 256
        with pytest.raises(ValueError) as excinfo:
            client_disconnect_message = ClientDisconnectMessage(player_id)
            assert "currently: 256" in str(excinfo.value)
            
    def test_serialize(self):
        client_disconnect_message = ClientDisconnectMessage(0)
        serialized_data = client_disconnect_message.serialize()
        assert serialized_data == b''
        
    def test_deserialize(self):
        serialized_data = b''
        client_disconnect_message = ClientDisconnectMessage.deserialize(serialized_data, 0)
        assert client_disconnect_message.player_id == 0
        
        serialized_data = b''
        with pytest.raises(TypeError) as excinfo:
            client_disconnect_message = ClientDisconnectMessage.deserialize(serialized_data)
        assert "not be None" in str(excinfo.value)
        
        serialized_data = b'\x00'
        with pytest.raises(ValueError) as excinfo:
            client_disconnect_message = ClientDisconnectMessage.deserialize(serialized_data, 0)
            assert "currently: 1" in str(excinfo.value)