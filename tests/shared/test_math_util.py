#!/usr/bin/env python3

"""Script contains tests for the math_util script"""

import math
import shared.game.math_util as math_util

class TestMathUtil():
    """Class testing math_util functions."""

    def test_get_random_direction_vector(self):
        vector = math_util.get_random_direction_vector()
        assert 0.999 < vector[0] * vector[0] + vector[1] * vector[1]  < 1.001

    def test_check_collision_of_circles(self):
        assert math_util.check_collision_of_circles([1,2], 2, [5,3], 3)
        assert not math_util.check_collision_of_circles([1,2], 2, [6,3], 3)
    
    def test_get_cartesian_coordinate(self):
        assert math_util.get_cartesian_coordinate([1., 2.], 3., 2.) == [-0.24844050964142728, 4.727892280477045]

    def test_get_polar_coordinate(self):
        assert math_util.get_polar_coordinate([1., 2.], [0., 2.]) == (math.pi, 1.)

    def test_get_vector_from_point_to_point(self):
        assert math_util.get_vector_from_point_to_point([1., 2.], [10., 5.]) == [9., 3.]
    
    def test_normalize_vector(self):
        assert math_util.normalize_vector([3., 4.]) == [3/5, 4/5]
    
    def test_mirror_vector(self):
        assert math_util.mirror_vector([1., -1.], [0., 1.]) == [1., 1.]

    def test_get_angle_from_message_angle(self):
        assert math_util.get_angle_from_message_angle(4, 2, 0.5) == math.pi * 1.25

    def test_get_message_angle_from_angle(self):
        assert math_util.get_message_angle_from_angle(4, math.pi * 1.25) == 0.5

    def test_check_direction_of_collision(self):
        assert math_util.check_direction_of_collision([0, 0], [0, 1], [0, 2])
        assert math_util.check_direction_of_collision([0, 0], [1, 1], [0, 2])
        assert not math_util.check_direction_of_collision([0, 0], [0, -1], [0, 2])
        assert not math_util.check_direction_of_collision([0, 0], [1, -1], [0, 2])
