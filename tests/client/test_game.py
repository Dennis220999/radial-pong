#!/usr/bin/env python3

"""Script contains tests for the server game script"""

import math

from client.game.game import Game
from shared.serialization.connection_confirmation_message import ConnectionStatus

class TestGame():
    """Class testing class Game."""

    def test_update_player_angle(self, test_client_game_running_participating: Game):
        game = test_client_game_running_participating
        game.update_player_angle(0.)
        my_player = game._Game__game_state.players[game._Game__player_index]
        assert my_player.platform.angle == 2/3 * math.pi

    def test_request_game_start(self, test_client_game_not_running: Game):
        game = test_client_game_not_running
        game.request_game_start()
        assert True
    
    def test_connect_failed(self, test_client_game_not_connected: Game):
        game = test_client_game_not_connected
        game._Game__connect(ConnectionStatus.FAILED, 42)
        assert game._Game__connected == False
    
    def test_connect_no_running_game(self, test_client_game_not_connected: Game):
        game = test_client_game_not_connected
        game._Game__connect(ConnectionStatus.SUCCESSFUL_WAITING, 42)
        assert game._Game__connected == True
        assert game._Game__in_game == False
        assert game._Game__game_is_running == False
        assert game._Game__player_id == 42

    def test_connect_running_game(self, test_client_game_not_connected: Game):
        game = test_client_game_not_connected
        game._Game__connect(ConnectionStatus.SUCCESSFUL_RUNNING, 42)
        assert game._Game__connected == True
        assert game._Game__in_game == False
        assert game._Game__game_is_running == True
        assert game._Game__player_id == 42

    def test_create_game_state(self, test_client_game_not_running: Game):
        game = test_client_game_not_running
        game._Game__create_game_state([1, 42, 3])
        assert game._Game__game_state != None
        assert len(game._Game__game_state.players) == 3
        assert game._Game__player_index == 1

    def test_update_balls(self, test_client_game_running_participating: Game):
        game = test_client_game_running_participating
        game._Game__update_balls([(18001, 30001), (40001, 44001), (50001, 32001), (41001, 43001)])
        balls = game._Game__game_state.balls
        assert len(balls) == 4
        assert balls[0].position == [18001, 30001]
        assert balls[1].position == [40001, 44001]
        assert balls[2].position == [50001, 32001]
        assert balls[3].position == [41001, 43001]
        game._Game__update_balls([(18002, 30002)])
        assert len(balls) == 1
        assert balls[0].position == [18002, 30002]

    def test_update_players(self, test_client_game_running_participating: Game):
        game = test_client_game_running_participating
        print(game._Game__game_state.players[0].remaining_lives)
        game._Game__update_players([0., 0.7, 1.], [8, 7, 6])
        players = game._Game__game_state.players
        assert players[0].remaining_lives == 8
        assert players[1].remaining_lives == 7
        assert players[2].remaining_lives == 6
        assert players[0].platform.angle == 0
        assert players[1].platform.angle == math.pi
        assert players[2].platform.angle == 2 * math.pi
