#!/usr/bin/env python3

""" Script contains tests for game scene script from client"""

import client.gui.game_scene as game_scene
from shared.game.game_components import Player, PlayerPlatform
import numpy as np
import pytest

class TestGameScene():
    """Tests class \"GameScene\"."""
    
    def test_compute_tranformation_matrix(self):
        game_board_center = [2**15, 2**15]
        player_index = 0
        player_count = 2
        screen_size = (960, 540)
        
        transformation_matrix = game_scene.GameScene._GameScene__compute_tranformation_matrix(game_board_center, 
                                                                                              player_index, 
                                                                                              player_count, 
                                                                                              screen_size)
        correct_matrix = np.array([[ -8.23987e-3,           0, 750.0041 ],
                                   [           0, -8.23987e-3, 540.0041 ],
                                   [           0,           0,           1 ]])
        np.testing.assert_allclose(transformation_matrix, correct_matrix, atol=1e-8)
        
    def test_compute_rotation_angle(self):
        player_index = 0
        player_count = 2
        rotation_angle = game_scene.GameScene._GameScene__compute_rotation_angle(player_index, player_count)
        np.testing.assert_allclose(rotation_angle, np.pi, atol=1e-8)
        
        player_index = 2
        player_count = 4
        rotation_angle = game_scene.GameScene._GameScene__compute_rotation_angle(player_index, player_count)
        np.testing.assert_allclose(rotation_angle, np.pi/4, atol=1e-8)
        
    def test_compute_rotation_matrix(self):
        player_index = 0
        player_count = 2
        rotation_matrix = game_scene.GameScene._GameScene__compute_rotation_matrix(player_index, player_count)
        correct_matrix = np.array([[ -1,  0, 0 ],
                                   [  0, -1, 0 ],
                                   [  0,  0, 1 ]])
        np.testing.assert_allclose(rotation_matrix, correct_matrix, atol=1e-8)
        
        player_index = 2
        player_count = 4
        rotation_matrix = game_scene.GameScene._GameScene__compute_rotation_matrix(player_index, player_count)
        correct_matrix = np.array([[ 0.70710678, -0.70710678, 0 ],
                                   [ 0.70710678,  0.70710678, 0 ],
                                   [          0,           0, 1 ]])
        np.testing.assert_allclose(rotation_matrix, correct_matrix, atol=1e-8)
    
    def test_compute_scaling_matrix(self):
        screen_width = 960
        screen_height = 540
        scaling_matrix = game_scene.GameScene._GameScene__compute_scaling_matrix(screen_width, screen_height)
        correct_matrix = np.array([[ 0.00823987,          0, 0 ],
                                   [          0, 0.00823987, 0 ],
                                   [          0,          0, 1 ]])
        np.testing.assert_allclose(scaling_matrix, correct_matrix, atol=1e-8)
        
        screen_width = 1280
        screen_height = 720
        scaling_matrix = game_scene.GameScene._GameScene__compute_scaling_matrix(screen_width, screen_height)
        correct_matrix = np.array([[ 0.01098649,          0, 0 ],
                                   [          0, 0.01098649, 0 ],
                                   [          0,          0, 1 ]])
        np.testing.assert_allclose(scaling_matrix, correct_matrix, atol=1e-8)
        
    def test_compute_translation_matrix(self):
        x = -10
        y = 20
        translation_matrix = game_scene.GameScene._GameScene__compute_translation_matrix(x, y)
        correct_matrix = np.array([[ 1, 0, -10 ],
                                   [ 0, 1,  20 ],
                                   [ 0, 0,   1 ]])
        np.testing.assert_allclose(translation_matrix, correct_matrix, atol=1e-8)
        
        x = 10
        y = -20
        translation_matrix = game_scene.GameScene._GameScene__compute_translation_matrix(x, y)
        correct_matrix = np.array([[ 1, 0,  10 ],
                                   [ 0, 1, -20 ],
                                   [ 0, 0,   1 ]])
        np.testing.assert_allclose(translation_matrix, correct_matrix, atol=1e-8)
        
    def test_compute_scaling_factor(self):
        screen_width = 960
        screen_height = 540
        scaling_factor = game_scene.GameScene._GameScene__compute_scaling_factor(screen_width, screen_height)
        np.testing.assert_allclose(scaling_factor, 0.00823987, atol=1e-8)
        
        screen_width = 1280
        screen_height = 720
        scaling_factor = game_scene.GameScene._GameScene__compute_scaling_factor(screen_width, screen_height)
        np.testing.assert_allclose(scaling_factor, 0.01098649, atol=1e-8)
        
    def test_compute_player_platform_position(self):
        player = Player(platform=PlayerPlatform(angle=0, radius=4000, offset=2500), player_id=0)
        game_board_center = [2**15, 2**15]
        game_board_radius = 25000
        platform_position = game_scene.GameScene._GameScene__compute_player_platform_position(player, game_board_center, 
                                                                                              game_board_radius)
        correct_position = np.array([2**15+25000+2500, 2**15, 1])
        np.testing.assert_allclose(platform_position, correct_position, atol=1e-8)
        
        player = Player(platform=PlayerPlatform(angle=np.pi/2, radius=4000, offset=2500), player_id=0)
        platform_position = game_scene.GameScene._GameScene__compute_player_platform_position(player, game_board_center, 
                                                                                              game_board_radius)
        correct_position = np.array([2**15, 2**15+25000+2500, 1])
        np.testing.assert_allclose(platform_position, correct_position, atol=1e-8)
        
    def test_compute_player_platform_intersection(self):
        game_board_center = [2**15, 2**15]
        game_board_radius = 25000
        platform_center_vec = np.array([2**15+25000+2500, 2**15])
        platform_radius = 4000
        platform_offset = 2500
        
        angle, intersection_vec_1, intersection_vec_2 = game_scene.GameScene._GameScene__compute_player_platform_intersection(game_board_center, 
                                                                                                                              game_board_radius,
                                                                                                                              platform_center_vec, 
                                                                                                                              platform_radius, 
                                                                                                                              platform_offset)
        correct_angle = 1.67501121
        correct_intersection_vec_1 = np.array([ 57590.72727273, 29796.0964444 ])
        correct_intersection_vec_2 = np.array([ 57590.72727273, 35739.9035556 ])
        np.testing.assert_allclose(angle, correct_angle, atol=1e-8)
        np.testing.assert_allclose(intersection_vec_1, correct_intersection_vec_1, atol=1e-8)
        np.testing.assert_allclose(intersection_vec_2, correct_intersection_vec_2, atol=1e-8)

        platform_center_vec = np.array([2**15, 2**15+25000+2500])
        angle, intersection_vec_1, intersection_vec_2 = game_scene.GameScene._GameScene__compute_player_platform_intersection(game_board_center, 
                                                                                                                              game_board_radius,
                                                                                                                              platform_center_vec, 
                                                                                                                              platform_radius, 
                                                                                                                              platform_offset)
        correct_angle = 1.67501121
        correct_intersection_vec_1 = np.array([ 35739.9035556, 57590.72727273 ])
        correct_intersection_vec_2 = np.array([ 29796.0964444, 57590.72727273 ])
        np.testing.assert_allclose(angle, correct_angle, atol=1e-8)
        np.testing.assert_allclose(intersection_vec_1, correct_intersection_vec_1, atol=1e-8)
        np.testing.assert_allclose(intersection_vec_2, correct_intersection_vec_2, atol=1e-8)