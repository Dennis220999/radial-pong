#!/usr/bin/env python3

""" Script contains tests for window scripts from client"""

from tests.conftest import STARTUP_TIME
import time

import pytest
from client.gui.window import Window
from client.gui.scene_information import SceneType

class TestWindow:
    """Tests for window class and sub-classes.
    
    Scene tests must run interactive, so they aren't part of the normal CI.
    
    """
    @pytest.mark.only_interactive
    def test_update_scene_information(self, dummy_game, dummy_scene_information):
        window = Window(dummy_game)
        window.update_scene_information(dummy_scene_information.start_scene_information())
        assert window._Window__scene.scene_type == SceneType.START_SCENE
        window.update_scene_information(dummy_scene_information.stop_scene_information())
        assert window._Window__scene.scene_type == SceneType.STOP_SCENE

    @pytest.mark.only_interactive
    def test_stop_scene(self, dummy_game, dummy_scene_information):
        window = Window(dummy_game)
        window.update_scene_information(dummy_scene_information.stop_scene_information())
        window.start()
        window.stop()
    
    @pytest.mark.only_interactive
    def test_game_scene(self, dummy_game, dummy_scene_information):
        window = Window(dummy_game)
        window.update_scene_information(dummy_scene_information.game_scene_information())
        window.start()
        window.stop()

    @pytest.mark.only_interactive
    def test_start_scene(self, test_client_game_not_connected, dummy_scene_information):
        test_client_game_not_connected.start()
        test_client_game_not_connected.window.update_scene_information(dummy_scene_information.start_scene_information())
        time.sleep(2* STARTUP_TIME)
        test_client_game_not_connected.stop()
    
    @pytest.mark.only_interactive
    def test_waiting_scene(self, dummy_game, dummy_scene_information):
        window = Window(dummy_game)
        window.update_scene_information(dummy_scene_information.waiting_scene_information())
        window.start()
        time.sleep(2 * STARTUP_TIME)
        window.stop()
