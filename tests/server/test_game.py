#!/usr/bin/env python3

"""Script contains tests for the server game script"""

import math
from tests.conftest import STARTUP_TIME
import time
from server.game.game import Game
from shared.game.game_components import *
from shared.serialization.connection_confirmation_message import ConnectionStatus


class TestGame():
    """Class testing class Game."""
    def test_create_connection_confirmation_message(self, test_server_game_not_running: Game):
        game = test_server_game_not_running
        message = game._Game__create_connection_confirmation_message(42)
        assert message.player_id == 42
        assert message.connection_status == ConnectionStatus.SUCCESSFUL_WAITING


    def test_create_game_start_message(self, test_server_game_not_running: Game):
        game = test_server_game_not_running
        message = game._Game__create_game_start_message()
        assert message.player_ids == game._Game__connected_players


    def test_create_game_stop_message(self, test_server_game_not_running: Game):
        game = test_server_game_not_running
        winner_properties = ([], None)
        message = game._Game__create_game_stop_message(winner_properties)
        assert message != None


    def test_create_game_state_message(self, test_server_game_running: Game):
        game = test_server_game_running
        message = game._Game__create_game_state_message()
        game_state = game._Game__game_state
        assert len(message.ball_positions) == len(game_state.balls)
        assert len(message.player_angles) == len(game_state.players)
        assert len(message.player_lives) == len(game_state.players)
        

    def test_update(self):
        #TODO
        assert True


    def test_add_player(self, test_server_game_not_running: Game):
        game = test_server_game_not_running
        game._Game__add_player(42)
        assert game._Game__connected_players[-1] == 42
    

    def test_remove_player(self, test_server_game_not_running: Game):
        game = test_server_game_not_running
        number_of_players = len(game._Game__connected_players)
        game._Game__connected_players.append(42)
        game._Game__remove_player(42)
        assert len(game._Game__connected_players) == number_of_players


    def test_update_player_input(self, test_server_game_running: Game):
        game = test_server_game_running
        game_state = game._Game__game_state
        player_id = game_state.players[0].player_id
        game._Game__update_player_input(player_id, 0.5)
        number_of_players = len(game_state.players)
        assert game_state.players[0].platform.angle == (2 * math.pi / number_of_players) * 0.5


    def test_ready_to_start(self, test_server_game_not_running: Game, test_server_game_running: Game):
        game_not_running = test_server_game_not_running
        game_running = test_server_game_running
        assert game_not_running._Game__ready_to_start()
        assert not game_running._Game__ready_to_start()
    

    def test_set_up(self, test_server_game_not_running: Game):
        game = test_server_game_not_running
        game._Game__set_up()
        game_state = game._Game__game_state
        assert game_state != None
        assert len(game_state.players) == len(game._Game__connected_players)


    def test_update_gamestate(self, test_server_game_running: Game):
        game = test_server_game_running
        game._Game__update_gamestate(1.)
        assert True #TODO check properly


    def test_move_ball(self, test_server_game_running: Game):
        game = test_server_game_running
        start_position = [1,2]
        angle = math.pi/4
        ball = Ball([start_position[0], start_position[1]], [math.cos(angle), math.sin(angle)])
        game._Game__move_ball(ball, 1.)
        assert ball.position == [start_position[0] + math.cos(angle) * ball.velocity, start_position[1] + math.sin(angle) * ball.velocity]


    def test_calculate_collisions(self, test_server_game_running: Game):
        game = test_server_game_running
        game._Game__calculate_collisions(game._Game__game_state.balls[0])
        assert True #TODO check properly
    

    def test_process_collision_with_circle(self, test_server_game_running: Game):
        game = test_server_game_running
        ball = Ball([200., 250.], [0., -1.])
        game._Game__process_collision_with_circle(ball, [300., 0.], 200.)
        assert ball.velocity_vector == [-0.689655172413793, 0.7241379310344827]

    
    def test_process_collision_with_inner_circle(self, test_server_game_running: Game):
        game = test_server_game_running
        ball = Ball([200., 250.], [0., -1.])
        game._Game__process_collision_with_circle(ball, [300., 0.], 200.)
        assert ball.velocity_vector == [-0.689655172413793, 0.7241379310344827]

    
    def test_check_win_condition(self, test_server_game_running: Game):
        game = test_server_game_running
        game._Game__check_win_condition()
        assert game._Game__game_is_running == True
        for i in range(game._Game__game_state.number_of_players - 1):
            game._Game__game_state.players[i].remaining_lives = 0
        game._Game__check_win_condition()
        assert game._Game__game_is_running == False


    def test_check_if_ball_is_lost(self, test_server_game_running: Game):
        game = test_server_game_running
        ball = game._Game__game_state.balls[0]
        ball.position[0] = 100000
        start_position = ball.position.copy()
        game._Game__check_if_ball_is_lost(ball)
        assert ball.position != start_position
        
        
    def test_get_winner_properties(self, test_server_game_running: Game):
        game = test_server_game_running
        winner_ids, winner_lives = game._Game__get_winner_properties()
        assert winner_ids == [1, 2, 3]
        assert winner_lives == 5
        
        game._Game__game_state.players[0].remaining_lives = 0
        game._Game__game_state.players[1].remaining_lives = 0
        game._Game__game_state.players[2].remaining_lives = 2
        winner_ids, winner_lives = game._Game__get_winner_properties()
        assert winner_ids == [3]
        assert winner_lives == 2


    def test_game_loop_with_start_and_stop(self, test_server_game_not_running: Game):
        game = test_server_game_not_running
        game._Game__set_up()
        game._Game__start_game_loop()
        time.sleep(2 * STARTUP_TIME)
        game._Game__stop()
        assert game._Game__game_is_running == False
