#!/usr/bin/env python3

"""Implements an abstract network handler, which is specialized on client and server side.
"""

import selectors
from socket import SHUT_RDWR
import threading

from shared.network.listener import Listener


class BaseNetworkHandler():
    READ_WRITE_EVENT = selectors.EVENT_READ | selectors.EVENT_WRITE
    def __init__(self, serializer: 'Serializer') -> None:
        """Initialize BaseNetworkHandler with serializer and structure for handler and listener.

        Args:
            serializer (Serializer): serializer instance
        
        Raises:
            NotImplementedError: BaseNetworkHandler can not be instanced due to its abstract character
        """
        if not self.__check_specialized():
            raise NotImplementedError("BaseNetworkHandler can not be instanced.")
        self.__serializer = serializer
        
        # set handler attributes
        self.__handler_thread = None
        self.__condition = threading.Condition()
        
        # set listener attributes
        self.__listener = None
        self.__listener_thread = None
        
        # set selector and sock 
        self.__selector = selectors.DefaultSelector()
        self._sock = None
        self._running = False
        
        # list for receive and send queues
        self._send_buffers = list()
        self._recv_buffers = list()
    
    def _start_handler(self) -> None:
        """Starts the run method in a seperate thread.
        """     
        self.__handler_thread = threading.Thread(target=self.run, name="Handler-Thread")
        self.__handler_thread.start()
    
    def _start_listener(self) -> None:
        """Starts the listener in a separate daemon thread.
        """
        self.__listener = Listener(self._recv_buffers, self.__condition, self.__serializer)
        self.__listener_thread = threading.Thread(target=self.__listener.listen, name="Listener-Thread")
        self.__listener_thread.daemon = True
        self.__listener_thread.start()
    
    def __check_specialized(self) -> bool:
        """Checks if the object is of type BaseNetworkHandler.

        Returns:
            bool: True if object is not of type BaseNetworkHandler, otherwise False
        """
        return type(self) != BaseNetworkHandler
        
    def run(self) -> None:
        """Abstract method to be run in another thread.
        
        Raises:
            NotImplementedError: method is not implemented for BaseNetworkHandler
        """
        raise NotImplementedError("Run method not implemented for object of type BaseNetworkHandler.")
    
    def send(self) -> None:
        """Abstract method to send message.
        
        Raises:
            NotImplementedError: method is not implemented for BaseNetworkHandler
        """
        raise NotImplementedError("Send method not implemented for object of type BaseNetworkHandler.")
        
    
    def _send_message(self, content: bytes, player_id: int) -> None:
        """Finally append message to the send queue.

        Args:
            content (bytes): serialized content to send
            player_id (int): id of player to send message to
        
        Raises:
            IndexError: player not registered
        """
        if player_id < len(self._send_buffers):
            self._send_buffers[player_id].put(content)
        else:
            raise IndexError("Player with ID %d not registered." % player_id)
    
    def __del__(self) -> None:
        """Deconstructor for network handler. Closes all connections and afterwards the sockets.
        """
        self.close_connection()
    
    def close_connection(self) -> None:
        """Executed on closing. This method can be specialized in the sub-classes.
        """
        self._close()
    
    def _close(self) -> None:
        """Signals the handler thread and listener thread to stop and waits for them to join. Additionally the selector and socket are closed.
        
        Checking that the attributes are not None is neccesarry due to the call of the deconstructor, if the initialization fails and thereby the attributes are not set to the specific values. 
        """
        # signal handler thread to stop
        self._running = False
        
        # signal listener to stop
        if self.__listener is not None:
            self.__listener.stop()

        # join handler thread
        if self.__handler_thread is not None and self.__handler_thread != threading.current_thread():
            self.__handler_thread.join(timeout=1)

        # join listener thread
        if self.__listener_thread is not None and self.__listener_thread != threading.current_thread():
            self.__listener_thread.join(timeout=1)

        # close selector and socket
        self.__selector.close()

        if self._sock is not None:
            self._sock.close()
            self._sock = None

    @property
    def serializer(self) -> 'Serializer':
        """Getter for serializer.

        Returns:
            Serializer: serializer instance
        """
        return self.__serializer

    @property
    def listener(self) -> Listener:
        """Getter for listener.

        Returns:
            Listener: initialized Listener object, to run in another thread (Listener.listen)
        """
        return self.__listener

    @property
    def condition(self) -> threading.Condition:
        """Getter for condition object.

        Returns:
            Condition: threading.Condition the listener should wait for
        """
        return self.__condition

    @property
    def selector(self) -> selectors.DefaultSelector:
        """Getter for selector object.

        Returns:
            selectors.DefaultSelector: current selector instance
        """
        return self.__selector
