#!/usr/bin/env python3

"""Implements an abstract connection handler."""

from zlib import crc32
import selectors
import threading
from queue import Queue
from typing import Tuple, Union
import socket

ENDIANESS = "little"

class BaseConnectionHandler():
    """Implements a basic request handler for a single connection between server
    and client."""

    PACKAGE_NUMBER_BYTES = 2
    PACKAGE_LENGTH_BYTES = 2
    PACKAGE_CRC_BYTES = 4
    PACKAGE_HEADER_LENGTH = PACKAGE_NUMBER_BYTES + PACKAGE_LENGTH_BYTES + PACKAGE_CRC_BYTES
    SKIP_COUNT_LOST_MESSAGE = 5 # number of trials to receive a message, after it is skipped 
    def __init__(self, selector: selectors.DefaultSelector, sendq: Queue, recvq: Queue, condition: threading.Condition) -> None:
        """Initialize general connection handler.

        Args:
            selector(selectors.DefaultSelector): multiplexer object for socket
            sendq(queue.Queue): queue to take messages to send from
            recvq(queue.Queue): queue to put received messages to
            condition(threading.Condition): condition to notify on message receive
        """
        self.__sel = selector
        self.__sendq = sendq
        self.__recvq = recvq
        self.__recv_counter = 0
        self.__send_counter = 0
        self.__message_queue = list() # stores messages that were received too early
        self.__recv_buffer = b""
        self.__condition = condition
        self.__skip_counter = 0
        
    @classmethod
    def compose(cls, content_data: bytes, package_number: int) -> bytearray:
        """Composes a package from the provided data. The result is a valid 
        package including the package number, package length, a checksum of 
        the content and the content itself.

        Args:
            content_data(bytes): serialized data
            package_number(int): number of package
        
        Return:
            package(bytearray): composed package with 8 Byte header
        """
        # compose package header
        package = bytearray()
        package += package_number.to_bytes(cls.PACKAGE_NUMBER_BYTES, ENDIANESS)
        package += len(content_data).to_bytes(cls.PACKAGE_LENGTH_BYTES, ENDIANESS)
        package += crc32(content_data).to_bytes(cls.PACKAGE_CRC_BYTES, ENDIANESS)

        # add package content and return
        package += content_data
        return package

    @classmethod
    def decompose(cls, package: bytes) -> Tuple[int,int,int,bytes]:
        """ Decomposes a package.
            
        Args:
            package(bytes): bytearray received from socket
        Returns:
            package_counter(int): number of decomposed package
            package_length(int): length of decomposed content bytes array
            checksum(int): CRC32 checksum
            content(bytes): serialized content
        """
        package_counter = int.from_bytes(package[:cls.PACKAGE_NUMBER_BYTES], ENDIANESS)
        package_len = int.from_bytes(package[cls.PACKAGE_NUMBER_BYTES:(cls.PACKAGE_NUMBER_BYTES+cls.PACKAGE_LENGTH_BYTES)], ENDIANESS)
        checksum = int.from_bytes(package[(cls.PACKAGE_NUMBER_BYTES+cls.PACKAGE_LENGTH_BYTES):(cls.PACKAGE_HEADER_LENGTH)], ENDIANESS)
        content = package[cls.PACKAGE_HEADER_LENGTH:]
        return package_counter, package_len, checksum, content
    
    def _check_for_complete_message(self) -> Tuple[bool, int]:
        """Checks if one complete message is stored in the buffer and returns 
        length of this message.

        Returns:
            Tuple[bool, int]: tuple of status on complete message available 
                (True: complete message available, False: no complete message 
                available) and length of available message(if first element is 
                False the second element is 0, otherwise it holds the actual 
                length of the content)
        """
        if len(self.__recv_buffer) >= self.PACKAGE_HEADER_LENGTH:
            # complete header available
            header = self.__recv_buffer[:self.PACKAGE_HEADER_LENGTH]
            _, package_len, _, _ = BaseConnectionHandler.decompose(header)
            if len(self.__recv_buffer[self.PACKAGE_HEADER_LENGTH:]) >= package_len:
                # complete message available
                return True, package_len
            else:
                return False, 0
        else:
            return False, 0
              
    def _check_crc(self, checksum: int, content: bytes) -> bool:
        """Compare the locally determined checksum to the received one. Return bool
        on equality.

        Args:
            checksum (int): checksum included in message
            content (bytes): content bytes of message

        Returns:
            bool: returns True on equality (no error) and False (failed CRC)
        """
        return checksum == crc32(content)
    
    def _on_failed_crc(self) -> None:
        """Is called when CRC failed. Increments object's receive counter.
        """
        self.__increment_recv_counter()         
    
    def _store_future_message(self, package_counter: int, content: bytes) -> None:
        """Stores messages that were received to early in a queue.

        Args:
            package_counter (int): number of package
            content (bytes): decomposed content
        """
        self.__message_queue.append((package_counter, content))
        if self.__skip_counter < self.SKIP_COUNT_LOST_MESSAGE:
            self.__skip_counter += 1
        else:
            # skip lost message
            self.__increment_recv_counter()
            self.__skip_counter = 0
            self.__put_prev_messages()
    
    def _store_current_message(self, content: bytes) -> None:
        """Puts decomposed content of to right time received message to output 
        queue, which is emptied by the listener. Increments object's receive
        counter and checks if messages in waiting queue may be put to output 
        queue.

        Args:
            content (bytes): decomposed content
        """
        self.__recvq.put(content)
        self.__increment_recv_counter()
        self.__skip_counter = 0

        # check for messages in buffer
        self.__put_prev_messages()
    
    def _notify_listener(self) -> None:
        """Notifies the listener thread via the condition.
        """
        with self.__condition: # acquire lock for threading.Condition object
            self.__condition.notify()
              
    def _process_complete_message(self, package_len: int) -> None:
        """Processes a message from the bytes buffer of the given package length.

        Args:
            package_len (int): length of the message to extract from the buffer
        """
        # extract message from buffer
        message_end = self.PACKAGE_HEADER_LENGTH + package_len
        message = self.__recv_buffer[:message_end]
        self.__recv_buffer = self.__recv_buffer[message_end:]

        # decompose message
        package_counter, package_len, checksum, content = BaseConnectionHandler.decompose(message)

        # check crc
        crc_status = self._check_crc(checksum, content)
        if not crc_status:
            self._on_failed_crc()
            return 
        
        # check package number 
        if package_counter > self.__recv_counter:
            # append message that is received too early to a waiting 
            # queue for future appending to the recvq
            self._store_future_message(package_counter, content)
            return
        elif package_counter < self.__recv_counter:
            # omit message 
            return

        # store message in recv queue if it is valid
        self._store_current_message(content)

        # notify listener
        self._notify_listener()
    
    def _send_package(self, data_to_send: bytes, sock: socket.socket) -> Union[int, None]:
        """Sends the given data, increments object's send counter and returns 
        number of sent bytes.

        Args:
            data_to_send (bytes): content to send 
            sock (socket.socket): socket to use for sending

        Returns:
            int: number of sent bytes
            None: socket was closed
        """
        composed_package = BaseConnectionHandler.compose(data_to_send, 
                                    self.__send_counter)
        try:
            bytes_send = sock.send(composed_package)
        except OSError:
            # socket was already closed by network handler
            return None
        self.__increment_send_counter()
        return bytes_send
    
    def _recv_data(self, sock: socket.socket) -> Union[bytes, None]:
        """Reiceives data and returns it. Closes socket if ConnectionResetError
        occurs.

        Args:
            sock (socket.socket): socket to use for receiving

        Returns:
            Union[bytes, None]: returns received data or None, if connection was 
                reset
        """
        try:
            recv_data = sock.recv(1024)
        except (ConnectionResetError, ConnectionAbortedError):
            # connection was closed by partner
            self.__sel.unregister(sock)
            sock.close()
            return None
        return recv_data
    
    def handle(self, key: selectors.SelectorKey, mask: int) -> None:
        """Handles incoming and outgoing connection requests for opened sockets.

        Args:
            key(selectors.SelectorKey): object from selector 
            mask(int): mask for socket events (see selectors) 
        """ 
        sock = key.fileobj
        if mask & selectors.EVENT_READ:
            # receive data from client
            recv_data = self._recv_data(sock)

            if recv_data:
                # received message from client

                self.__recv_buffer += recv_data
            
                message_available = True
                while message_available:
                    message_available, package_length = self._check_for_complete_message()

                    if message_available:
                        self._process_complete_message(package_length)
               
        if mask & selectors.EVENT_WRITE and not self.__sendq.empty():
            # send data to client
            data_to_send = self.__sendq.get()
            _ = self._send_package(data_to_send, sock)
    
    def __put_prev_messages(self) -> None:
        """Puts previously received message, that matches the current package 
        counter to the recvq.
        """
        for idx, tup in enumerate(self.__message_queue):
            package_no, message = tup
            if package_no == self.__recv_counter:
                self.__recvq.put(message)
                self._notify_listener()
                self.__increment_recv_counter()
                del self.__message_queue[idx]
                self.__put_prev_messages()
                break        
    
    def __increment_recv_counter(self) -> None:
        """Increments recv counter and avoids overflow over 2**(PACKAGE_NUMBER_BYTES*8) 
        (refer to protocol description).
        """
        self.__recv_counter = (self.__recv_counter + 1) % (2**(self.PACKAGE_NUMBER_BYTES*8))
    
    def __increment_send_counter(self) -> None:
        """Increments send counter and avoids overflow over 2**(PACKAGE_NUMBER_BYTES*8) 
        (refer to protocol description).
        """
        self.__send_counter = (self.__send_counter + 1) % (2**(self.PACKAGE_NUMBER_BYTES*8))

    @property
    def condition(self) -> threading.Condition:
        """Getter for condition attribute.

        Returns:
            threading.Condition: threading.Condition object to notify if a message is received
            None: no condition attached
        """
        return self.__condition
    
    @condition.setter
    def condition(self, condition: threading.Condition) -> None:
        """Setter condition attribute.

        Args:
            condition(threading.condition): condition object to notify if message is received
        
        Raises:
            TypeError:  condition is not of type threading.Condition
        """
        if not isinstance(condition, threading.Condition):
            raise TypeError("Argument must be of type threading.Condition.")

        self.__condition = condition
    
