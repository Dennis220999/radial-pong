#!/usr/bin/env python3

"""Script implements all game components to rerpesent the state of a game."""

import math
from typing import List

import shared.game.math_util as math_util


class Ball(object):
    """Represents a ball in a game.
    
    Args:
        position (List[float]): Position of the ball.
        velocity_vector (List[float]): Velocity vector of the ball with length 1.

    Attributes:
        position (List[float]): Position of the ball.
        velocity_vector (List[float]): Velocity vector of the ball with length 1.
        velocity (float): Velocity of the ball.
        RADIUS (float): Radius of the ball.
    """

    START_VELOCITY = 7000.
    SPEED_UP_PER_SECOND = 100
    __RADIUS = 1000.
    __MAX_CONSECUTIVE_BORDER_BOUNCES = 10
    __RANDOM_VELOCITY_VECTOR_INFLUENCE = 0.33

    def __init__(self, position: List[float], velocity_vector: List[float]):
        self.position = position
        self.velocity_vector = velocity_vector
        self.velocity = self.START_VELOCITY
        self.__consecutive_border_bounces = 0
    
    
    @property
    def position(self) -> List[float]:
        """
        Getter-method for attribute \"position\".

        Returns:
            List[float]: Position of the ball.
        """
        return self.__position

    @position.setter
    def position(self, position: List[float]) -> None:
        """
        Setter-method for attribute \"position\".

        Args:
            position (List[float]): New position of the ball
        """
        self.__position = position.copy()

    @property
    def velocity(self) -> float:
        """
        Getter-method for attribute \"velocity\".

        Returns:
            float: Velocity of the ball.
        """
        return self.__velocity

    @velocity.setter
    def velocity(self, velocity: float) -> None:
        """
        Setter-method for attribute \"velocity\".

        Args:
            velocity float: New velocity of the ball
        """
        self.__velocity = velocity

    @property
    def velocity_vector(self) -> List[float]:
        """
        Getter-method for attribute \"velocity_vector\".

        Returns:
            List[float]: Velocity vector of the ball
        """
        return self.__velocity_vector

    @velocity_vector.setter
    def velocity_vector(self, velocity_vector: List[float]) -> None:
        """
        Setter-method for attribute \"velocity_vector\".

        Args:
            velocity_vector (List[float]): New velocity vector of the ball
        """
        square_length = velocity_vector[0]**2 + velocity_vector[1]**2
        if abs(1 - square_length) < 0.0001: # Check if the velocity_vector has the length 1
            self.__velocity_vector = velocity_vector.copy()
        else:
            length = math.sqrt(square_length)
            self.__velocity_vector = [velocity_vector[0] / length, velocity_vector[1] / length] # Scale the velocity_vector to 1

    @property
    def RADIUS(self) -> float:
        """
        Getter-method for attribute \"RADIUS\".

        Returns:
            float: Radius of the ball.
        """
        return self.__RADIUS
    
    @property
    def MAX_CONSECUTIVE_BORDER_BOUNCES(self) -> int:
        """
        Getter-method for attribute \"MAX_CONSECUTIVE_BORDER_BOUNCES\".

        Returns:
            int: Maximum number of consecutive border bounces, until a random component is added to the moving vector
        """
        return self.__MAX_CONSECUTIVE_BORDER_BOUNCES
    
    @property
    def RANDOM_VELOCITY_VECTOR_INFLUENCE(self) -> float:
        """
        Getter-method for attribute \"RANDOM_VELOCITY_VECTOR_INFLUENCE\".

        Returns:
            float: Influence ratio of the random velocity vector w.r.t the actual velocity vector
        """
        return self.__RANDOM_VELOCITY_VECTOR_INFLUENCE
    
    @property
    def consecutive_border_bounces(self) -> int:
        """
        Getter-method for attribute \"consecutive_border_bounces\".

        Returns:
            int: Number of consecutive border bounces
        """
        
        return self.__consecutive_border_bounces
    
    def bounce_with_border(self) -> None:
        """Increases attibute \"consecutive_border_collisions\" by one."""
        
        self.__consecutive_border_bounces += 1
    
    def bounce_with_platform(self) -> None:
        """Resets attibute \"consecutive_border_collisions\" to zero."""

        self.__consecutive_border_bounces = 0

class PlayerPlatform(object):
    """Represents a platform of a player in a game.
    
    Args:
        angle (float): Angle of the platform relative to the center of the game field.
        radius (float): Radius of the curve of the platform.
        offset (float): Distance from the center of the circle rerpesenting the platform to the outer line of the game field.

    Attributes:
        angle (float): Angle of the platform relative to the center of the game field.
        radius (float): Radius of the curve of the platform.
        offset (float): Distance from the center of the circle rerpesenting the platform to the outer line of the game field.
    """

    __MAX_PLATFORM_RADIUS = 16000
    __PLATFORM_OFFSET_FACTOR = 0.625

    def __init__(self, angle: float, radius: float, offset: float):
        self.angle = angle
        self.radius = radius
        self.offset = offset
    
    @classmethod
    def create_platform(cls, angle: float, player_count: int) -> 'PlayerPlatform':
        """Creates a Player Platform with the size based on the number of players.
        
        Args:
            angle (float): Angle of the platform relative to the center of the game field.
            player_count (int): Nummber of players.
        
        Returns:
            PlayerPlatform: New Player Platform
        """
        radius = cls.__MAX_PLATFORM_RADIUS / player_count
        offset = radius * cls.__PLATFORM_OFFSET_FACTOR
        return PlayerPlatform(angle, radius, offset)

    @property
    def angle(self) -> float:
        """
        Getter-method for attribute \"angle\".

        Returns:
            float: Angle of the platform relative to the center of the game field in radians.
        """
        return self.__angle

    @angle.setter
    def angle(self, angle: float) -> None:
        """
        Setter-method for attribute \"angle\".

        Args:
            angle (float): Angle of the platform relative to the center of the game field in radians.
        """
        self.__angle = angle

    
    @property
    def radius(self) -> float:
        """
        Getter-method for attribute \"radius\".

        Returns:
            float: Radius of the curve of the platform.
        """
        return self.__radius

    @radius.setter
    def radius(self, radius: float) -> None:
        """
        Setter-method for attribute \"radius\".

        Args:
            radius (float): New radius of the curve of the platform.
        """
        if radius < 0:
            self.__radius = 0
        else:
            self.__radius = radius

    
    @property
    def offset(self) -> float:
        """
        Getter-method for attribute \"offset\".

        Returns:
            float: Distance from the center of the circle rerpesenting the platform to the outer line of the game field.
        """
        return self.__offset

    @offset.setter
    def offset(self, offset: float) -> None:
        """
        Setter-method for attribute \"offset\".

        Args:
            offset (float): New distance from the center of the circle rerpesenting the platform to the outer line of the game field.
        """
        self.__offset = offset


class Player(object):
    """Rerpesenting a player in a game.

    Args:
        platform (PlayerPlatform): Platform of the player.
        player_id (int): Id of the player.

    Args:
        platform (PlayerPlatform): Platform of the player.
        player_id (int): Id of the player.
        INITIAL_LIVES (int): Number of lives the player starts with.
        remaining_lives (int): Number of lives the player currently has.
    """

    __INITIAL_LIVES = 5

    def __init__(self, platform: PlayerPlatform, player_id: int):
        self.__platform = platform
        self.__player_id = player_id
        self.remaining_lives = self.INITIAL_LIVES

    def is_alive(self) -> bool:
        """Checks if player is alive.

        Returns:
            bool: Player is alive.
        """
        return self.remaining_lives > 0
    
    @property
    def platform(self) -> PlayerPlatform:
        """
        Getter-method for attribute \"platform\".

        Returns:
            PlayerPlatform: Platform of the player.
        """
        return self.__platform

    @property
    def player_id(self) -> int:
        """
        Getter-method for attribute \"player_id\".

        Returns:
            int: Id of the player.
        """
        return self.__player_id

    @property
    def remaining_lives(self) -> int:
        """
        Getter-method for attribute \"remaining_lives\".

        Returns:
            float: Number of lives the player currently has.
        """
        return self.__remaining_lives
    
    @remaining_lives.setter
    def remaining_lives(self, remaining_lives: int) -> None:
        """
        Setter-method for attribute \"remaining_lives\".

        Args:
            remaining_lives (int): New number of lives the player has.
        """
        if remaining_lives < 0:
            self.__remaining_lives = 0
        else:
            self.__remaining_lives = remaining_lives
    
    @property
    def INITIAL_LIVES(self) -> int:
        """
        Getter-method for attribute \"INITIAL_LIVES\".

        Returns:
            int: Number of lives the player starts with.
        """
        return self.__INITIAL_LIVES


class GameState(object):
    """Represents a state of a game.
    
    Args:
        number_of_players (int): Number of slots for players int the game
    
    Attributes:
        number_of_players (int): Number of slots for players int the game
        players (List(Player)): All players in the game.
        balls (List(Ball)): All balls in the game.
        RADIUS (float): Radius of the game field.
        CENTER (List[float]): Center position of the game field.
        boundary_radius (float): Radius of the boundarys between players.
    """

    __RADIUS = 25000.
    __CENTER = [32768., 32768.]
    __MAX_BOUNDARY_RADIUS = 4000

    def __init__(self, number_of_players: int):
        self.__number_of_players = number_of_players
        self.__players = []
        self.__balls = []
        self.__boundary_radius = self.__MAX_BOUNDARY_RADIUS / self.__number_of_players


    def add_player(self, player_id: int):
        """Adds a player to the game state.
        
        Args:
            player_id (int): Id of the player
        """
        if len(self.players) >= self.number_of_players:
            return
        angle = math_util.get_angle_from_message_angle(self.__number_of_players, len(self.players), 0.5)
        platform = PlayerPlatform.create_platform(angle, self.__number_of_players)
        player = Player(platform, player_id)
        self.__players.append(player)


    @property
    def number_of_players(self) -> int:
        """
        Getter-method for attribute \"number_of_players\".

        Returns:
            int: Number of slots for players int the game
        """
        return self.__number_of_players

    @property
    def players(self) -> List[Player]:
        """
        Getter-method for attribute \"players\".

        Returns:
            List[Player]: All players in the game.
        """
        return self.__players
    
    @property
    def balls(self) -> List[Ball]:
        """
        Getter-method for attribute \"balls\".

        Returns:
            List[Ball]: All balls in the game.
        """
        return self.__balls

    @property
    def boundary_radius(self) -> float:
        """
        Getter-method for attribute \"boundary_radius\".

        Returns:
            float: Radius of the boundarys between players.
        """
        return self.__boundary_radius

    @property
    def RADIUS(self) -> float:
        """
        Getter-method for attribute \"RADIUS\".

        Returns:
            float: Radius of the game field.
        """
        return self.__RADIUS

    @property
    def CENTER(self) -> List[float]:
        """
        Getter-method for attribute \"CENTER\".

        Returns:
            List[float]: Center position of the game field.
        """
        return self.__CENTER
