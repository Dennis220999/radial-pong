#!/usr/bin/env python3

import struct
from typing import List, Union

from shared.serialization.serializable_message_type import SerializableMessageType


class GameStartMessage(SerializableMessageType):
    """Implements message being sent from server to client to indicate the start of a game."""
    
    # identifier for message type, according to the transmission protocol
    _MESSAGE_TYPE_ID = 4
    
    def __init__(self, player_ids: List[int]) -> None:
        """
        Initializes GameStartMessage.

        Args:
            player_ids (List[int]): Players' identifiers [0, 255], max. 255 elements
        """
        
        self.player_ids = player_ids
        
    @property
    def player_ids(self) -> List[int]:
        """
        Getter-method for attribute \"player_ids\".

        Returns:
            List[int]: Players' identifiers [0, 255], max. 255 elements
        """
        
        return self.__player_ids
    
    @player_ids.setter
    def player_ids(self, player_ids: List[int]) -> None:
        """
        Setter-method for attribute \"player_ids\".

        Args:
            player_ids (List[int])): Players' identifiers [0, 255], max. 255 elements

        Raises:
            ValueError: Player's identifier must be in range [0, 255]; only 255 ids allowed
        """
        
        # check boundaries for player count
        upper_boundary = 2**(8*self._PLAYER_COUNT_BYTES)-1
        if len(player_ids) < 1 or len(player_ids) > upper_boundary:
            raise ValueError("Argument \"player_ids\" must have [%d, %d] elements (currently: %d)!" % 
                             (1, upper_boundary, len(player_ids)))
    
        # check boundaries for single player_ids
        upper_boundary = 2**(8*self._PLAYER_ID_BYTES)-1
        for player_id in player_ids:
            if player_id < 0 or player_id > upper_boundary:
                raise ValueError("Argument \"player_ids\" must be in range [0, %d] (currently: %d)!" % (upper_boundary, 
                                                                                                    player_id))
        
        self.__player_ids = player_ids

    def serialize(self) -> bytes:
        """
        Serializes encapsulated data.

        Returns:
            bytes: Serialized data
        """
        
        # get number of players
        player_count = len(self.__player_ids)
        # cast python integer type to ctypes 1 byte unsigned integer type (abbr. "B") and get byte-wise data 
        # representation
        player_count_bytes = struct.pack("B", player_count)
        
        # for each player id, cast python integer type to ctypes 1 byte unsigned integer type (abbr. "B") and get 
        # byte-wise data representation
        player_ids_bytes = struct.pack("%dB" % player_count, *self.__player_ids)
        
        # return concatenation of bytes, according to the transmission protocol
        return player_count_bytes + player_ids_bytes
    
    @classmethod
    def deserialize(cls, serialized_data: bytes, player_id: Union[int, None] = None) -> 'GameStartMessage':
        """
        Deserializes bytes and encapsulates actual data.
        
        Args:
            serialized_data (bytes): Serialized data
            player_id (int, optional): Player's identifier [0, 255]
            
        Raises:
            ValueError: Byte count of given data must not deceed/exceed expectations, according to the transmission 
            protocol (2-256 bytes); transmitted value of player_ids must be valid (in range [0, 255])
            
        Returns:
            GameStartMessage: Deserialized data, being encapsulated in class instance
        """
        
        # check, if byte count of given data does not deceed/exceed expectations according to the transmission protocol
        lower_boundary = cls._PLAYER_COUNT_BYTES + cls._PLAYER_ID_BYTES
        upper_boundary = cls._PLAYER_COUNT_BYTES + cls._PLAYER_ID_BYTES*(2**(8*cls._PLAYER_COUNT_BYTES) - 1)
        if len(serialized_data) < lower_boundary or len(serialized_data) > upper_boundary:
            raise ValueError("Argument \"serialized_data\" must be [%d, %d] bytes (currently: %d)!" 
                             % (lower_boundary, upper_boundary, len(serialized_data)))
        
        # according to the transmission protocol, player count is the first and player_ids the subsequent bytes
        # bytes are implicitely casted to python integers
        # check validity of player count (must be in range [1, 255] - max. 255 is already implecitely restricted by 1 
        # byte unsigned byte type)
        if serialized_data[0] < 1:
            raise ValueError("Transmitted value of \"player_count\" (currently: %d) is out of range [%d, %d]!" % 
                             (serialized_data[0], 1, 2**(8*cls._PLAYER_COUNT_BYTES) - 1))
        # validity of player ids (range [0, 255]) is implicitely given by 1 byte unsigned integer type
        # check, if player count matches number of remaining bytes
        if serialized_data[0] != len(serialized_data[1:]):
            raise ValueError("Transmitted value of \"player_count\" (currently: %d) doesn't match number of transmitted\
                             player_ids (currently: %d)!" % 
                             (serialized_data[0], len(serialized_data[1:])))
        
        # take list of player ids from serialized data
        player_ids = list(struct.unpack("%dB" % serialized_data[0], serialized_data[1:]))
        
        # return new class instance, which encapsulates the deserialized data
        return GameStartMessage(player_ids)