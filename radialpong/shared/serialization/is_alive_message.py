#!/usr/bin/env python3

from typing import Union

from shared.serialization.serializable_message_type import SerializableMessageType

class IsAliveMessage(SerializableMessageType):
    """"""

    _MESSAGE_TYPE_ID = 10

    def __init__(self, player_id : int=0) -> None:
        self.player_id = player_id
    
    @property
    def player_id(self) -> int:
        """
        Getter-method for attribute \"player_id\".

        Returns:
            int: Player's identifier [0, 255]
        """
        
        return self.__player_id
    
    @player_id.setter
    def player_id(self, player_id: int) -> None:
        """
        Setter-method for attribute \"player_id\".

        Args:
            player_id (int)): Player's identifier [0, 255]

        Raises:
            ValueError: Player's identifier must be in range [0, 255]
        """
    
        # # check boundaries for player_id
        # upper_boundary = 255
        # if player_id < 0 or player_id > upper_boundary:
        #     raise ValueError("Argument \"player_id\" must be in range [0, %d] (currently: %d)!" % (upper_boundary, player_id))
        
        self.__player_id = player_id

    def serialize(self) -> bytes:
        return bytes()
    
    @classmethod
    def deserialize(cls, serialized_data: bytes, player_id: Union[int, None]) -> 'SerializableMessageType':
        if len(serialized_data) != 0:
            raise ValueError("Argument \"serialized_data\" must be 0 bytes (currently: %d)!" % len(serialized_data))

        return IsAliveMessage(player_id)