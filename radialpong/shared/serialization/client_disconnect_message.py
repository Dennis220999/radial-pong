#!/usr/bin/env python3

from typing import Union

from shared.serialization.serializable_message_type import SerializableMessageType


class ClientDisconnectMessage(SerializableMessageType):
    """Implements message being sent from client to server to indicate disconnection."""
    
    # identifier for message type, according to the transmission protocol
    _MESSAGE_TYPE_ID = 8
    
    def __init__(self, player_id: int=0) -> None:
        """
        Initializes ClientDisconnectMessage.
        
        Args:
            player_id (int, optional): Player's identifier [0, 255] (default 0)
        """
        
        self.player_id = player_id
        
    @property
    def player_id(self) -> int:
        """
        Getter-method for attribute \"player_id\".

        Returns:
            int: Player's identifier [0, 255]
        """
        
        return self.__player_id
    
    @player_id.setter
    def player_id(self, player_id: int) -> None:
        """
        Setter-method for attribute \"player_id\".

        Args:
            player_id (int)): Player's identifier [0, 255]

        Raises:
            ValueError: Player's identifier must be in range [0, 255]
        """
    
        # check boundaries for player_id
        upper_boundary = 255
        if player_id < 0 or player_id > upper_boundary:
            raise ValueError("Argument \"player_id\" must be in range [0, %d] (currently: %d)!" % (upper_boundary, 
                                                                                                   player_id))
        
        self.__player_id = player_id
        
    def serialize(self) -> bytes:
        """
        Serializes encapsulated data.

        Returns:
            bytes: Serialized data
        """
        
        # there is no data, return empty bytes object
        return bytes()
    
    @classmethod
    def deserialize(cls, serialized_data: bytes, player_id: Union[int, None] = None) -> 'ClientDisconnectMessage':
        """
        Deserializes bytes and encapsulates actual data.
        
        Args:
            serialized_data (bytes): Serialized data
            player_id (int, optional): Player's identifier [0, 255]
            
        Raises:
            ValueError: Byte count of given data must match expectations, according to the transmission protocol 
            (0 bytes)
            TypeError: Player id is required
            
        Returns:
            ClientDisconnectMessage: Deserialized data, being encapsulated in class instance
        """
        
        # check, if byte count of given data matches expectations according to the transmission protocol
        if len(serialized_data) != 0:
            raise ValueError("Argument \"serialized_data\" must be 0 bytes (currently: %d)!" % len(serialized_data))
        
        # check, if player id is given
        if player_id is None:
            raise TypeError("Argument \"player_id\" must not be None!")
        
        # return new class instance
        return ClientDisconnectMessage(player_id)