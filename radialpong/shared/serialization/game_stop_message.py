#!/usr/bin/env python3

import math
import struct
from typing import List, Union

from shared.serialization.serializable_message_type import SerializableMessageType


class GameStopMessage(SerializableMessageType):
    """Implements message being sent from server to client to inform about game stop."""
    
    # identifier for message type, according to the transmission protocol
    _MESSAGE_TYPE_ID = 5
    
    def __init__(self, winner_ids: List[int], winner_lives: Union[int, None]) -> None:
        """
        Initializes GameStopMessage.
        
        Args:
            winner_ids (List[int]): Winners' identifiers [0, 255], max. 255 elements
            winner_lives (Union[int, None]): Winners' number of remaining lives [1, 255]
        """
        
        self.winner_ids = winner_ids
        self.winner_lives = winner_lives
        
    @property
    def winner_ids(self) -> List[int]:
        """
        Getter-method for attribute \"winner_ids\".

        Returns:
            List[int]: Winners' identifiers [0, 255], max. 255 elements
        """
        
        return self.__winner_ids
    
    @winner_ids.setter
    def winner_ids(self, winner_ids: List[int]) -> None:
        """
        Setter-method for attribute \"winner_ids\".

        Args:
            winner_ids (List[int])): Winners' identifiers [0, 255], max. 255 elements

        Raises:
            ValueError: Winner's identifier must be in range [0, 255]; only 255 ids allowed
        """
        
        # check boundaries for winner count
        upper_boundary = 2**(8*self._PLAYER_COUNT_BYTES)-1
        if len(winner_ids) > upper_boundary:
            raise ValueError("Argument \"winner_ids\" must have [%d, %d] elements (currently: %d)!" % 
                             (0, upper_boundary, len(winner_ids)))
    
        # check boundaries for single winner_ids
        upper_boundary = 2**(8*self._PLAYER_ID_BYTES)-1
        for winner_id in winner_ids:
            if winner_id < 0 or winner_id > upper_boundary:
                raise ValueError("Argument \"winner_ids\" must be in range [0, %d] (currently: %d)!" % (upper_boundary, 
                                                                                                    winner_id))
        
        self.__winner_ids = winner_ids
    
    @property
    def winner_lives(self) -> Union[int, None]:
        """
        Getter-method for attribute \"winner_lives\".

        Returns:
            Union[int, None]: Winners' number of remaining lives [1, 255]
        """
        
        return self.__winner_lives
    
    @winner_lives.setter
    def winner_lives(self, winner_lives: Union[int, None]) -> None:
        """
        Setter-method for attribute \"winner_lives\".

        Args:
            winner_lives (Union[int, None]): Winners' number of remaining lives [1, 255]

        Raises:
            ValueError: Lives count must be in range [1, 255]
        """
        
        if winner_lives is not None:
            # check boundaries for lives argument
            upper_boundary = 2**(8*self._PLAYER_LIVES_BYTES)-1
            if winner_lives < 1 or winner_lives > upper_boundary:
                    raise ValueError("Argument \"winner_lives_count\" must be in range [%d, %d] (currently: %d)!" 
                                    % (1, upper_boundary, winner_lives))
        
        self.__winner_lives = winner_lives
        
    def serialize(self) -> bytes:
        """
        Serializes encapsulated data.

        Returns:
            bytes: Serialized data
        """
        
        winner_count = len(self.__winner_ids)
        winner_lives_available = self.__winner_lives is not None
        
        if (winner_count > 0) != winner_lives_available:
            raise ValueError("Number of elements in \"winner_ids\" (currently: %d) must match the availability of the\
                             attribute \"winner_lives\" (currently: %s)!"
                             % (winner_count, winner_lives_available))
        
        # cast python integer type to ctypes 1 byte unsigned integer type (abbr. "B") and get byte-wise data 
        # representation
        winner_count_bytes = struct.pack("B", winner_count)
        
        # for each winner id, cast python integer type to ctypes 1 byte unsigned integer type (abbr. "B") and get 
        # byte-wise data representation
        winner_ids_bytes = struct.pack("%dB" % winner_count, *self.__winner_ids)
        
        # cast python integer type to ctypes 1 byte unsigned integer type (abbr. "B") and get byte-wise data 
        # representation
        if winner_lives_available:
            winner_lives_bytes = struct.pack("B", self.__winner_lives)
        else:
            winner_lives_bytes = b''
        
        return winner_count_bytes + winner_ids_bytes + winner_lives_bytes
    
    @classmethod
    def deserialize(cls, serialized_data: bytes, player_id: Union[int, None] = None) -> 'GameStopMessage':
        """
        Deserializes bytes and encapsulates actual data.
        
        Args:
            serialized_data (bytes): Serialized data
            player_id (int, optional): Player's identifier [0, 255]
            
        Raises:
            ValueError: Byte count of given data must match expectations, according to the transmission protocol 
            (0 bytes)
            
        Returns:
            GameStopMessage: Deserialized data, being encapsulated in class instance
        """
        
        # check, if byte count of given data does not deceed/exceed expectations according to the transmission protocol
        # min. 0 winning winners
        lower_boundary = cls._PLAYER_COUNT_BYTES
        # max. 255 winning winners
        upper_boundary = (cls._PLAYER_COUNT_BYTES + cls._PLAYER_ID_BYTES * (2**(8*cls._PLAYER_COUNT_BYTES) - 1)
                          + cls._PLAYER_LIVES_BYTES)
        if len(serialized_data) < lower_boundary or len(serialized_data) > upper_boundary:
            raise ValueError("Argument \"serialized_data\" must be [%d, %d] bytes (currently: %d)!" 
                             % (lower_boundary, upper_boundary, len(serialized_data)))
            
        # according to the transmission protocol, winner count is the first and winner_ids, winner_lives are the 
        # subsequent bytes objects
        # bytes are implicitely casted to python integers
        start_idx = 0
        winners_count = serialized_data[start_idx]
        start_idx += 1
        # check, if winners count matches number of remaining winner bytes
        expected_bytes_count = winners_count * cls._PLAYER_ID_BYTES + (cls._PLAYER_LIVES_BYTES if winners_count > 0 else 0)
        actual_bytes_count = len(serialized_data[start_idx:])
        if expected_bytes_count != actual_bytes_count:
            raise ValueError("Expected value of remaining bytes (currently: %d) doesn't match number of transmitted\
                             remaining bytes (currently: %d)!" 
                             % (expected_bytes_count, actual_bytes_count))
        # get winner_ids_bytes
        winner_ids_bytes_count = winners_count * cls._PLAYER_ID_BYTES
        winner_ids_bytes = serialized_data[start_idx:start_idx+winner_ids_bytes_count]
        # get ctypes 1 byte unsigned integer (abbr. "B") from byte-wise data representation (automatically cast to 
        # python integer type)
        winner_ids = list(struct.unpack("%dB" % winners_count, winner_ids_bytes))
        # get winner_lives byte, if available
        # bytes are implicitely casted to python integers
        start_idx += winner_ids_bytes_count
        if winners_count > 0:
            winner_lives = serialized_data[start_idx]
        else:
            winner_lives = None
        
        # return new class instance, which does not encapsulate any data
        return GameStopMessage(winner_ids, winner_lives)
    