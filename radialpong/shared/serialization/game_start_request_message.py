#!/usr/bin/env python3

from typing import Union

from shared.serialization.serializable_message_type import SerializableMessageType


class GameStartRequestMessage(SerializableMessageType):
    """Implements message being sent from client to server to request game start."""
    
    # identifier for message type, according to the transmission protocol
    _MESSAGE_TYPE_ID = 3
    
    def __init__(self) -> None:
        """Initializes GameStartRequestMessage."""
        
        pass
        
    def serialize(self) -> bytes:
        """
        Serializes encapsulated data.

        Returns:
            bytes: Serialized data
        """
        
        # there is no data, return empty bytes object
        return bytes()
    
    @classmethod
    def deserialize(cls, serialized_data: bytes, player_id: Union[int, None] = None) -> 'GameStartRequestMessage':
        """
        Deserializes bytes and encapsulates actual data.
        
        Args:
            serialized_data (bytes): Serialized data
            player_id (int, optional): Player's identifier [0, 255]
            
        Raises:
            ValueError: Byte count of given data must match expectations, according to the transmission protocol 
            (0 bytes)
            
        Returns:
            GameStartRequestMessage: Deserialized data, being encapsulated in class instance
        """
        
        # check, if byte count of given data matches expectations according to the transmission protocol
        if len(serialized_data) != 0:
            raise ValueError("Argument \"serialized_data\" must be 0 bytes (currently: %d)!" % len(serialized_data))
        
        # return new class instance, which does not encapsulate any data
        return GameStartRequestMessage()