#!/usr/bin/env python3

import math
from typing import List, Tuple

import numpy as np
import pyglet
from pyglet.sprite import Sprite
from pyglet.graphics import OrderedGroup
import glooey
from glooey.containers import Stack, VBox, HBox

from client.gui.scene_information import SceneInformation, SceneType
from client.gui.scene import Scene
import shared.game.math_util as game_math

class GameScene(Scene):
    """Implements GameScene"""
    
    # number of render layers
    __RENDER_LAYER_COUNT = 5
    # size of the game layout in game space
    __GAME_SIZE = 2**16-1
    # label padding in game space
    __LABEL_PADDING = 4000
    # label font size
    __LABEL_FONT_SIZE = 20
    # message angle per mouse movement
    __ANGLE_PER_MOUSE_DX = 0.002
    
    def __init__(self, scene_information: SceneInformation, window: 'Window', game: 'Game', resources: 'pyglet.resource.Loader') -> None:
        """Initializes GameScene object.
        
        Args:
            scene_information(SceneInformation): provide a SceneInformation
            window (Window):
            game (Game):
            resources(pyglet.resource.Loader): central resource loader
            
        Raises:
            TypeError: GameScene object can only be initialized with scene information of type GAME_SCENE"""
        if scene_information.scene_type != SceneType.GAME_SCENE:
            raise TypeError("Provided scene information must be of type SceneType.GAME_SCENE but is of type %s" 
                            % scene_information.scene_type)
        super().__init__(scene_information, window, game, resources)


    def create(self) -> None:
        super().create()
        # push mouse motion handler to window's event handler stack
        self._window.push_handlers(on_mouse_motion=self.__move_platform)
        
        # OpenGL parameter adaption
        # line width (relevant for platform arcs)
        pyglet.gl.glLineWidth(5)

        # get centered images
        self.__GAME_BOARD_BACKGROUND = self._resources.image('game_board_background.png')
        self.__GAME_BOARD_BACKGROUND.anchor_x = self.__GAME_BOARD_BACKGROUND.width // 2
        self.__GAME_BOARD_BACKGROUND.anchor_y = self.__GAME_BOARD_BACKGROUND.height // 2
        self.__GAME_BOARD_OVERLAY = self._resources.image('game_board_overlay.png')
        self.__GAME_BOARD_OVERLAY.anchor_x = self.__GAME_BOARD_OVERLAY.width // 2
        self.__GAME_BOARD_OVERLAY.anchor_y = self.__GAME_BOARD_OVERLAY.height // 2
        self.__GAME_BOARD_BOUNDARY = self._resources.image('game_board_boundary.png')
        self.__GAME_BOARD_BOUNDARY.anchor_x = self.__GAME_BOARD_BOUNDARY.width // 2
        self.__GAME_BOARD_BOUNDARY.anchor_y = self.__GAME_BOARD_BOUNDARY.height // 2
        self.__GAME_BOARD_BALL = self._resources.image('game_board_ball.png')
        self.__GAME_BOARD_BALL.anchor_x = self.__GAME_BOARD_BALL.width // 2
        self.__GAME_BOARD_BALL.anchor_y = self.__GAME_BOARD_BALL.height // 2
        
        # define render layers
        self.__render_layers = [OrderedGroup(idx) for idx in range(self.__RENDER_LAYER_COUNT)]
        
        # get various parameters
        game_state = self._scene_information.game_state
        window_size = self._window.get_size()
        window_center = tuple([c/2 for c in window_size])
        self.__player_index = self._game.player_index
        self.__player_count = game_state.number_of_players
        
        # compute transformation matrix and scaling factor
        self.__transformation_matrix = self.__compute_tranformation_matrix(game_state.CENTER, self.__player_index, 
                                                                           self.__player_count, window_size)
        self.__scaling_factor = self.__compute_scaling_factor(*window_size)
        
        # set up gui containers
        self.__stack = Stack()
        self._gui.add(self.__stack)
        self.__labels_hbox = HBox()
        self.__stack.add(self.__labels_hbox)
        self.__opponent_lives_vbox = VBox()
        self.__opponent_lives_vbox.padding = self.__scaling_factor * GameScene.__LABEL_PADDING
        self.__labels_hbox.add(self.__opponent_lives_vbox, size='expand')
        self.__own_lives_vbox = VBox()
        self.__own_lives_vbox.padding = self.__scaling_factor * GameScene.__LABEL_PADDING
        self.__labels_hbox.add(self.__own_lives_vbox, size='expand')
        
        # set up game board
        self.__game_board_background = Sprite(self.__GAME_BOARD_BACKGROUND,
                                            x=self._window_scale[0] // 2,
                                            y=self._window_scale[1] // 2,
                                            batch=self._graphics_batch,
                                            group=self.__render_layers[0])
        self.__game_board_background.scale = self._window_scale[1] / self.__GAME_BOARD_BACKGROUND.height
        self.__game_board_overlay = Sprite(self.__GAME_BOARD_OVERLAY,
                                            x=self._window_scale[0] // 2,
                                            y=self._window_scale[1] // 2,
                                            batch=self._graphics_batch,
                                            group=self.__render_layers[4])
        self.__game_board_overlay.scale = self._window_scale[1] / self.__GAME_BOARD_OVERLAY.height
        
        # set up player-assigned elements (area limiters, platforms, lives lables)
        self.__player_area_limiters = list()
        self.__player_platforms = {player.player_id: None for player in game_state.players}
        self.__player_lives = {player.player_id: None for player in game_state.players}
        for idx, player in enumerate(game_state.players):
            # set up player area limiters
            self.__init_player_area_limiter(idx, game_state)
            
            # set up player platforms
            self.__init_player_platform(player, idx, game_state)
            
            # set up player lives
            self.__init_player_label(player, idx)
            
        # set up balls
        self.__balls = list()
        for ball in game_state.balls:
            self.__init_ball(ball)


    def _modify(self):
        game_state = self._scene_information.game_state
        
        for player in game_state.players:
            # update player platforms
            self.__update_player_platform(player, game_state)
            
            # update player lives
            self.__player_lives[player.player_id].set_text(("Player %d: " % player.player_id) + 
                                                           "♥" * player.remaining_lives)
        
        # update set of balls
        self.__update_set_of_balls(game_state)
        
        # actually update remaining balls
        for idx, ball in enumerate(game_state.balls[:len(self.__balls)]):
            self.__update_ball(ball, idx)
            
    def __init_player_area_limiter(self, player_idx: int, game_state: 'GameState') -> None:
        """
        Initializes and draws limiters that separate areas of players.

        Args:
            player_idx (int): Index of the player, whose limiter is to be drawn
            game_state (GameState): Initial game state
        """
        
        player_area_angle = 2 * math.pi / self.__player_count
        player_area_limiter_radius = self.__scaling_factor * game_state.boundary_radius
        
        angle = player_idx * player_area_angle
        game_pos = np.array(game_math.get_cartesian_coordinate(game_state.CENTER, 
                                                        game_state.RADIUS, angle) + [1.])
        screen_pos = self.__transformation_matrix @ game_pos

        player_area_limiter = Sprite(self.__GAME_BOARD_BOUNDARY,
                                x=screen_pos[0],
                                y=screen_pos[1],
                                batch=self._graphics_batch,
                                group=self.__render_layers[3])
        player_area_limiter.scale = 2 * player_area_limiter_radius / self.__GAME_BOARD_BOUNDARY.height
        self.__player_area_limiters.append(player_area_limiter)
            
    def __init_player_platform(self, player: 'Player', player_idx: int, game_state: 'GameState') -> None:
        """
        Initializes and draws player platforms.

        Args:
            player (Player): Player, whose platform is to be drawn
            player_idx (int): Index of the player, whose platform is to be drawn
            game_state (GameState): Initial game state
        """
        
        radius = self.__scaling_factor * player.platform.radius
        game_pos = np.array(self.__compute_player_platform_position(player, game_state.CENTER, game_state.RADIUS))
        screen_pos = self.__transformation_matrix @ game_pos
        angle, intersection_vec_1, intersection_vec_2 = self.__compute_player_platform_intersection(
                game_state.CENTER, game_state.RADIUS, game_pos[:2], player.platform.radius, player.platform.offset)
        rotation_angle_1, _ = game_math.get_polar_coordinate(game_pos[:2], intersection_vec_2)
        rotation_angle_2 = self.__compute_rotation_angle(self.__player_index, self.__player_count)
        player_platform = pyglet.shapes.Arc(screen_pos[0], screen_pos[1], radius, angle=angle,
                                        start_angle=rotation_angle_1 + rotation_angle_2,
                                        color=self._PLAYER_COLORS[player_idx], batch=self._graphics_batch,
                                        group=self.__render_layers[2])
        self.__player_platforms[player.player_id] = player_platform
        
    def __update_player_platform(self, player: 'Player', game_state: 'GameState') -> None:
        """
        Updates player platform, according to the new game state.

        Args:
            player (Player): Player, whose platform is to be drawn
            game_state (GameState): Initial game state
        """
        plattform = self.__player_platforms[player.player_id]
        if player.remaining_lives <= 0:
            plattform.visible = False
        else:
            plattform.visible = True
        
            player_count = game_state.number_of_players
            game_pos = np.array(self.__compute_player_platform_position(player, game_state.CENTER, game_state.RADIUS))
            screen_pos = self.__transformation_matrix @ game_pos
            angle, intersection_vec_1, intersection_vec_2 = self.__compute_player_platform_intersection(
                    game_state.CENTER, game_state.RADIUS, game_pos[:2], player.platform.radius, player.platform.offset)
            rotation_angle_1, _ = game_math.get_polar_coordinate(game_pos[:2], intersection_vec_2)
            rotation_angle_2 = self.__compute_rotation_angle(self.__player_index, player_count)
            plattform._x = screen_pos[0]
            plattform._y = screen_pos[1]
            plattform._start_angle = rotation_angle_1 + rotation_angle_2
            plattform._update_position()
            
    def __init_player_label(self, player: 'Player', player_idx: int) -> None:
        """
        Initializes and draws labels that show player ids and their remaining lives.

        Args:
            player (Player): Player, whose label is to be drawn
            player_idx (int): Index of the player, whose label is to be drawn
        """
        
        label = glooey.Label("Player %d: " % player.player_id + "♥" * player.remaining_lives, 
                                    font_size=self.__LABEL_FONT_SIZE, color=self._PLAYER_COLORS[player_idx])
        if player_idx == self.__player_index:
            label.set_alignment('right')
            self.__own_lives_vbox.add(label, size='expand')
        else:
            self.__opponent_lives_vbox.add(label, size=0)
        self.__player_lives[player.player_id] = label
        
    def __init_ball(self, ball: 'Ball') -> None:
        """
        Initializes and draws ball.

        Args:
            ball (Ball): Ball to be drawn
        """
        
        radius = self.__scaling_factor * ball.RADIUS
        game_pos = np.array(ball.position + [1.])
        screen_pos = self.__transformation_matrix @ game_pos
        ball = Sprite(self.__GAME_BOARD_BALL,
                    x=screen_pos[0],
                    y=screen_pos[1],
                    batch=self._graphics_batch,
                    group=self.__render_layers[1])
        ball.scale = 2 * radius / self.__GAME_BOARD_BALL.height
        self.__balls.append(ball)
        
    def __update_set_of_balls(self, game_state: 'GameState') -> None:
        """
        Adds/removes balls to/from set of balls, according to the new game state.

        Args:
            game_state (GameState): Game state with updated set of balls
        """
        
        # first check, if balls were removed -> remove their graphical representations as well
        if len(game_state.balls) < len(self.__balls):
            self.__balls = self.__balls[:len(game_state.balls)]
        # check, if balls were added -> add new graphical representations
        elif len(game_state.balls) > len(self.__balls):
            for ball in game_state.balls[len(self.__balls):]:
                self.__init_ball(ball)
        
    def __update_ball(self, ball: 'Ball', ball_idx: int) -> None:
        """
        Updates ball, according to the new game state.

        Args:
            ball (Ball): Ball to be updated
            ball_idx (int): Index of the ball
        """
        
        game_pos = np.array(ball.position + [1.])
        screen_pos = self.__transformation_matrix @ game_pos
        self.__balls[ball_idx]._x = screen_pos[0]
        self.__balls[ball_idx]._y = screen_pos[1]
        self.__balls[ball_idx]._update_position()
          
    def __move_platform(self, x: int, y: int, dx: int, dy: int) -> None:
        """
        Receives mouse position and moves platform accordingly.
        The event is dispatched every time the operating system registers a mouse movement. This is not necessarily once for every pixel moved – the operating system typically samples the mouse at a fixed frequency

        NOTE: When mouse-exclusive mode is enabled, x and y parameters are meaningless.

        Args:
            x(int): x-pos of mouse pointer
            y(int): y-pos of mouse pointer
            dx(int): distance the mouse travelled along x axis to get to its present position
            dy(int): distance the mouse travelled along y axis to get to its present position
        """
        
        current_message_angle = game_math.get_message_angle_from_angle(self.__player_count, 
                                                                       self._scene_information.game_state.players[self.__player_index].platform.angle)
        message_angle_change = self.__ANGLE_PER_MOUSE_DX * dx
        # apply change, clip at borders
        new_message_angle = min(max(current_message_angle + message_angle_change, 0.01), 0.99)

        self._game.update_player_angle(new_message_angle)

    @classmethod
    def __compute_tranformation_matrix(cls, game_board_center: List[float], player_index: int, player_count: int, 
                                       screen_size: Tuple[int, int]) -> np.ndarray:
        """Computes matrix that represents transformation from game space to screen space.

        Args:
            game_board_center (List[float]): Position of game board center in game space
            player_index (int): Index of local player
            player_count (int): Number of players
            screen_size (Tuple[int]): Size of the screen in px
            
        Returns:
            np.ndarray: Transformation matrix
        """
        
        # compute translation matrix that shifts the center of the game board to the origin of game space
        shift = tuple([-c for c in game_board_center])
        translation_to_origin_matrix = cls.__compute_translation_matrix(*shift)
        
        # compute rotation matrix that places the local player at the bottom of the screen
        rotation_matrix = cls.__compute_rotation_matrix(player_index, player_count)
        
        # compute scaling matrix that abstracts from screen resolution
        scaling_matrix = cls.__compute_scaling_matrix(*screen_size)
        
        # compute translation matrix that shifts the center of the game board to the center of screen space
        shift = tuple([c/2 for c in screen_size])
        translation_to_center_matrix = cls.__compute_translation_matrix(*shift)
        
        # compute whole transformation matrix by multiplication of single transformation matrices
        # note the way function composition for matrix multiplication works: the composition is evaluated from the right 
        # to the left
        return translation_to_center_matrix @ scaling_matrix @ rotation_matrix @ translation_to_origin_matrix
        
    @staticmethod
    def __compute_rotation_angle(player_index: int, player_count: int) -> float:
        """
        Computes rotation angle that places the local player at the bottom of the screen

        Args:
            player_index (int): Index of the local player (in range [0, ..., player_count])
            player_count (int): Number of players

        Returns:
            float: Rotation angle
        """
        
        player_area_range = 2 * math.pi / player_count
        # angle is to be centered in given player area
        shift = (player_index + 1/2) * player_area_range
        # player platform with index 0 should be placed at the bottom
        offset = 3 / 2 * math.pi
        
        return offset - shift
    
    @classmethod
    def __compute_rotation_matrix(cls, player_index: int, player_count: int) -> np.ndarray:
        """
        Computes rotation matrix that places the local player at the bottom of the screen

        Args:
            player_index (int): Index of the local player (in range [0, ..., player_count])
            player_count (int): Number of players

        Returns:
            np.ndarray: Rotation matrix
        """
        
        alpha = cls.__compute_rotation_angle(player_index, player_count)
        
        # standard 2D rotation matrix
        return np.array([[math.cos(alpha), -math.sin(alpha), 0],
                         [math.sin(alpha),  math.cos(alpha), 0],
                         [              0,                0, 1]])
        
    @classmethod
    def __compute_scaling_matrix(cls, screen_width: int, screen_height: int) -> np.ndarray:
        """
        Computes scaling matrix that transforms from game space to screen space.

        Args:
            screen_width (int): Horizontal screen resolution in px
            screen_height (int): Vertical screen resolution in px

        Returns:
            np.ndarray: Scaling matrix
        """
        
        # compute scaling factor
        scaling_factor = cls.__compute_scaling_factor(screen_width, screen_height)
        
        # standard 2D scaling matrix
        return np.array([[scaling_factor,              0, 0],
                         [             0, scaling_factor, 0],
                         [             0,              0, 1]])
        
    @staticmethod
    def __compute_translation_matrix(x: float, y: float) -> np.ndarray:
        """
        Computes translation matrix for translation by (x, y).

        Args:
            x (float): Translation in x direction
            y (float): Translation in y direction

        Returns:
            np.ndarray: Translation matrix
        """
        
        # standard 2D translation matrix
        return np.array([[1, 0, x],
                         [0, 1, y],
                         [0, 0, 1]])
        
    @classmethod
    def __compute_scaling_factor(cls, screen_width: int, screen_height: int) -> float:
        """
        Computes scaling factor that transforms from game space to screen space.

        Args:
            screen_width (int): Horizontal screen resolution in px
            screen_height (int): Vertical screen resolution in px

        Returns:
            float: Scaling factor
        """
        
        # game layout is quadratic and has to be incorporated into the available screen area -> smaller dimension
        min_screen_dimension = min(screen_width, screen_height)
        
        return min_screen_dimension / cls.__GAME_SIZE
    
    @staticmethod
    def __compute_player_platform_position(player: 'Player', game_board_center: List[float], 
                                           game_board_radius: float) -> np.ndarray:
        """
        Computes platform position of a given player in game space.

        Args:
            player (Player): Corresponsing player instance
            game_board_center (List[float]): Position of the game board center in game space
            game_board_radius (float): Radius of the game board in game space

        Returns:
            np.ndarray: Position of player's platform in game space in homogeneous coordinates
        """
        
        # get position on game board border in game space
        game_pos = game_math.get_cartesian_coordinate(game_board_center, game_board_radius, player.platform.angle)
        # 2D line equation from game board center to its border, in direction of platform
        return np.append(np.array(game_pos) + player.platform.offset * np.array(
            game_math.normalize_vector(game_math.get_vector_from_point_to_point(game_board_center, game_pos))), [1.])
        
    @staticmethod
    def __compute_player_platform_intersection(game_board_center: List[float], game_board_radius: float,
                                        platform_center_vec: np.ndarray, platform_radius: float, 
                                        platform_offset: float) -> Tuple[float, np.ndarray, np.ndarray]:
        """
        Computes intersection between game_board and given player platform (cf. https://stackoverflow.com/a/3349134).

        Args:
            game_board_center (List[float]): Game board center position in game space
            game_board_radius (float): Game board radius in game space
            platform_center_vec (np.ndarray): Platform center position in game space
            platform_radius (float): Platform radius in game space
            platform_offset (float): Platform offset in game space

        Returns:
            float: Angle between two intersection points in radians
            np.ndarray: Position of the first intersection point in game space
            np.ndarray: Position of the second intersection point in game space
        """
        
        game_board_center_vec = np.array(game_board_center)
        center_distance = game_board_radius + platform_offset
        center_distance_vec = platform_center_vec - game_board_center_vec
        a = (game_board_radius**2 - platform_radius**2 + center_distance**2) / (2*center_distance)
        h = math.sqrt(game_board_radius**2 - a**2)
        lens_center_vec = game_board_center_vec + a * center_distance_vec / center_distance
        
        intersection_vec_1 = np.array([
            lens_center_vec[0] + h * (center_distance_vec[1]) / center_distance,
            lens_center_vec[1] - h * (center_distance_vec[0]) / center_distance
        ])
        intersection_vec_2 = np.array([
            lens_center_vec[0] - h * (center_distance_vec[1]) / center_distance,
            lens_center_vec[1] + h * (center_distance_vec[0]) / center_distance
        ])
        
        intersection_secant_length = np.linalg.norm(intersection_vec_1 - intersection_vec_2)
        intersection_to_platform_center_distance_1 = np.linalg.norm(platform_center_vec - intersection_vec_1)
        intersection_to_platform_center_distance_2 = np.linalg.norm(platform_center_vec - intersection_vec_1)
        np.testing.assert_almost_equal(intersection_to_platform_center_distance_1, 
                                       intersection_to_platform_center_distance_2)
        angle = 2 * math.asin(intersection_secant_length / (2 * intersection_to_platform_center_distance_1))
        
        return angle, intersection_vec_1, intersection_vec_2
