#!/usr/bin/env python3

from typing import List, Tuple, Union
import pyglet
from glooey.containers import Stack
from glooey.images import Image

from client.gui.scene_information import SceneInformation, SceneType
from client.gui.scene import Scene
from client.gui.gui_elements import MessageLabel, CustomImageButton

class StopScene(Scene):
    """Initializes StopScene"""
    def __init__(self, scene_information: SceneInformation, window: 'Window', game: 'Game', resources: 'pyglet.resource.Loader') -> None:
        """Initializes StopScene object.
        
        Args:
            scene_information(SceneInformation): provide a SceneInformation
            window (Window):
            game (Game):
            resources(pyglet.resource.Loader): central resource loader
            
        Raises:
            TypeError: StopScene object can only be initialized with scene information of type STOP_SCENE"""
        if scene_information.scene_type != SceneType.STOP_SCENE:
            raise TypeError("Provided scene information must be of type SceneType.STOP_SCENE but is of type %s" 
                            % scene_information.scene_type)
        super().__init__(scene_information, window, game, resources)


    def create(self) -> None:
        super().create()
        # create stack that contains all widgets
        self.__stack = Stack()
        self._gui.add(self.__stack)

        # add background image
        self.__background = Image(image=self._resources.image('background.png'))
        self.__stack.add(self.__background)
        
        # create message label
        message_text, message_color = self.__create_label_text_and_color(self._scene_information.winner_ids, 
                                                                         self._scene_information.winner_indices, 
                                                                         self._scene_information.winner_lives)
        self.__label = MessageLabel(message_text, color=message_color, line_wrap=True)
        line_wrap_width = int(self._window.get_size()[0] * 0.8)
        self.__label.enable_line_wrap(line_wrap_width)
        self.__label.set_alignment('bottom')
        self.__label.set_padding(bottom=100)
        self.__stack.add(self.__label)

        self.__button = CustomImageButton(self._resources.image('return_button.png'), self._resources.image('return_button_down.png'))
        self.__button.push_handlers(on_click=lambda w: self._game.wait_for_other_players())
        self.__button.set_alignment('bottom')
        self.__button.set_padding(bottom=20)
        self.__stack.add(self.__button)


    def _modify(self) -> None:
        pass
    
    @classmethod
    def __create_label_text_and_color(cls, winner_ids: List[int], winner_indices: List[int], 
                                      winner_lives: Union[int, None]) -> Tuple[str, Tuple[int, int, int]]:
        """
        Creates label text about winners, depending on the number of them.

        Args:
            winner_ids (List[int]): Winners' identifiers
            winner_indices (List[int]): Winners' indices
            winner_lives (Union[int, None]): Winners' number of remaining lives

        Returns:
            str: Label text
        """
        
        if winner_lives is None or len(winner_ids) != 1:
            return "Game stopped.", (0, 0, 0)
        
        life_str = "life" if winner_lives == 1 else "lives"
        
        return ("Player %d won with %d remaining %s!" % (winner_ids[0], winner_lives, life_str), 
                cls._PLAYER_COLORS[winner_indices[0] % len(cls._PLAYER_COLORS)])
