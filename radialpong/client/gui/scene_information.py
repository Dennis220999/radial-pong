#!/usr/bin/env python3

from enum import Enum
from typing import List, Union
from shared.game.game_components import GameState, Player

class SceneType(Enum):
    START_SCENE = 0
    WAITING_SCENE = 1
    GAME_SCENE = 2
    STOP_SCENE = 3

class SceneInformation():
    """Implements interface for different types of scenes
    
    Attributes:
        __scene_type(SceneType): The type of scene information"""

    def __init__(self, scene_type : SceneType) -> None:
        """Virtual initializer for SceneInformation
        
        Args:
            scene_type(SceneType): The scene type of a concrete object
            
        Raises:
            NotImplementedError: Must be overridden for concrete SceneInformation classes
        """

        if type(self) is SceneInformation:
            raise NotImplementedError
        self.__scene_type = scene_type
    

    @property
    def scene_type(self) -> SceneType:
        """Getter for scene_type.
        
        Returns:
            SceneType: Scene type of object
            
        Raises:
            NotImplementedError: When base class is instantiated
        """
        if self is SceneInformation:
            raise NotImplementedError
        else:
            return self.__scene_type

class StartSceneInformation(SceneInformation):
    """Implements StartScene type."""

    def __init__(self, error_label_text : str = "") -> None:
        """Init for StartSceneInformation.

        Args:
            error_label_text (str, optional): Initial text for error label. Defaults to "".
        """
        super().__init__(SceneType.START_SCENE)
        self.__error_label_text = error_label_text
    
    @property
    def error_label_text(self) -> str:
        return self.__error_label_text


class WaitingSceneInformation(SceneInformation):
    """
    Implements WaitingScene information type.
    
    Args:
        player_count (int): Number of participating players
    """

    def __init__(self, player_count: int) -> None:
        super().__init__(SceneType.WAITING_SCENE)
        self.__player_count = player_count
        
    @property
    def player_count(self) -> int:
        """Getter-method for attribute \"player_count\".

        Returns:
            int: Number of participating players
        """
        
        return self.__player_count
    

class GameSceneInformation(SceneInformation):
    """Implements GameScene information type. """
    def __init__(self, game_state : GameState) -> None:
        """
        Initializes GameScene information.
        
        Args:
            game_state(GameState): GameStageMessage obejct
        """
        super().__init__(SceneType.GAME_SCENE)
        self.__game_state = game_state

    @property
    def game_state(self) -> GameState:
        """Getter for game state.
        
        Returns:
            GameState: Gamestate of GameScene instance"""
        return self.__game_state


class StopSceneInformation(SceneInformation):
    """Implements StopScene infromation type"""

    def __init__(self, winner_ids: List[int], winner_indices: List[int], winner_lives: Union[int, None]) -> None:
        """
        Initializes StopScene information.
        
        Args:
            winner_ids (List[int]): Winners' identifiers
            winner_indices (List[int]): Winners' indices
            winner_lives (Union[int, None]): Winners' number of remaining lives
        """
        super().__init__(SceneType.STOP_SCENE)
        self.winner_ids = winner_ids
        self.winner_indices = winner_indices
        self.winner_lives = winner_lives
    
    @property
    def winner_ids(self) -> List[int]:
        """
        Getter-method for attribute \"winner_ids\".

        Returns:
            List[int]: Winners' identifiers
        """
        
        return self.__winner_ids
    
    @winner_ids.setter
    def winner_ids(self, winner_ids: List[int]) -> None:
        """
        Setter-method for attribute \"winner_ids\".

        Args:
            winner_ids (List[int])): Winners' identifiers

        """

        self.__winner_ids = winner_ids
        
    @property
    def winner_indices(self) -> List[int]:
        """
        Getter-method for attribute \"winner_indices\".

        Returns:
            List[int]: Winners' indices
        """
        
        return self.__winner_indices
    
    @winner_indices.setter
    def winner_indices(self, winner_indices: List[int]) -> None:
        """
        Setter-method for attribute \"winner_indices\".

        Args:
            winner_indices (List[int])): Winners' indices

        """

        self.__winner_indices = winner_indices
    
    @property
    def winner_lives(self) -> Union[int, None]:
        """
        Getter-method for attribute \"winner_lives\".

        Returns:
            Union[int, None]: Winners' number of remaining lives
        """
        
        return self.__winner_lives
    
    @winner_lives.setter
    def winner_lives(self, winner_lives: Union[int, None]) -> None:
        """
        Setter-method for attribute \"winner_lives\".

        Args:
            winner_lives (Union[int, None]): Winners' number of remaining lives
        """
        
        self.__winner_lives = winner_lives
