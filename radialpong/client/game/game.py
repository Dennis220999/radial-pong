#!/usr/bin/env python3

"""Script implements the game class."""

from typing import List, Tuple, Union
import threading
import time


from shared.game.game_components import *
from shared.game import math_util
from shared.serialization.serializable_message_type import SerializableMessageType
from shared.serialization.connection_confirmation_message import ConnectionConfirmationMessage, ConnectionStatus
from shared.serialization.player_count_message import PlayerCountMessage
from shared.serialization.game_start_message import GameStartMessage
from shared.serialization.game_state_message import GameStateMessage
from shared.serialization.game_stop_message import GameStopMessage
from shared.serialization.client_input_message import ClientInputMessage
from shared.serialization.game_start_request_message import GameStartRequestMessage
from shared.serialization.is_alive_message import IsAliveMessage
from shared.serialization.connection_request_message import ConnectionRequestMessage
from client.serialization.serialization import Serializer
from client.network.network_handler import NetworkConfiguration
from client.gui.window import Window
from client.gui.scene_information import SceneType, StartSceneInformation, WaitingSceneInformation, GameSceneInformation, StopSceneInformation
from client.sound.sound_player import SoundPlayer


class Game:
    """Represents the game_state.
    
    Attributes:
        __game_state (GameState): The current game state
        __player_id (int): The player id of the player
        __player_index (int): Index of the player in the list in the game state
        __player_count (int): The number of participating players
        __connected (bool): The client is connected
        __in_game (bool): The client is in a running game
        __game_is_running (bool): A game is running
    """
    PING_INTERVAL = 4
    def __init__(self):
        self.__game_state = None
        self.__player_id = None
        self.__player_index = None
        self.__player_count = 0
        self.__connected = False
        self.__in_game = False
        self.__game_is_running = False
        self.__on_waiting_screeen = False
        self.__serializer = None
        self.__window = None
        self.__sound_player = None

        self.__ping_counter = 0
        self.__ping_thread = None

    
    def start(self):
        """Starts client"""
        self.__sound_player = SoundPlayer()
        self.__sound_player.start()
        self.__sound_player.play_title_music()
        self.__window = Window(self)
        self.__window.start()
        

    @property
    def serializer(self) -> 'Serializer':
        """
        Getter-method for attribute \"serializer\".

        Returns:
            Serializer: Serializer to communicate with the network.
        """
        return self.__serializer
    
    @serializer.setter
    def serializer(self, serializer: 'Serializer') -> None:
        """
        Setter-method for attribute \"serializer\".

        Args:
            serializer (Serializer): Serializer to communicate with the network.
        """
        self.__serializer = serializer

    @property
    def window(self) -> 'Window':
        """
        Getter-method for attribute \"window\".

        Returns:
            Window: Window representing the GUI 
        """
        return self.__window
    
    @window.setter
    def window(self, window: 'Window') -> None:
        """
        Setter-method for attribute \"window\".

        Args:
            window (Window): Window representing the GUI 
        """
        self.__window = window
        
    @property
    def player_index(self) -> Union[int, None]:
        """
        Getter-method for attribute \"player_indx\".

        Returns:
            int: Player index in ordered list of players
        """
        return self.__player_index
    
    @property
    def player_count(self) -> int:
        """
        Getter-method for attribute \"player_count\".

        Returns:
            int: Number of participating players
        """
        return self.__player_count

    @property
    def connected(self) -> bool:
        """Getter-method for connected status

        Returns:
            bool: connected
        """
        return self.__connected

    def update_player_angle(self, message_angle: float):
        """Updates the angle of the player.
        
        Args:
            message_angle (float): Angle of the player between 0 and 1
        """
        if self.__game_is_running and self.__in_game:
            angle = math_util.get_angle_from_message_angle(self.__game_state.number_of_players, self.__player_index, message_angle)
            self.__game_state.players[self.__player_index].platform.angle = angle
            self.serializer.serialize(ClientInputMessage(message_angle))


    def request_game_start(self):
        """Request a game start"""
        self.serializer.serialize(GameStartRequestMessage())
        
    def wait_for_other_players(self) -> None:
        """Shows/updates waiting scene while waiting for other players."""
        self.__on_waiting_screeen = True
        """Shows/updates waiting scene while waiting for other players. The server is pinged in a 4 second Interval. """
        if self.__ping_thread is None:
            self.__ping_thread = threading.Thread(None,self._ping,daemon=True, name="Ping-Thread")
            self.__ping_thread.start()
        self.__window.update_scene_information(WaitingSceneInformation(self.__player_count))

    def start_connection(self, network_config: 'NetworkConfiguration') -> bool:
        """Start a connection to a server and provide a status on connection 
        success.

        Args:
            network_config (NetworkConfiguration): Network configuration of the server (ip address and host)
        
        Return:
            bool: status of connection request (True: connection successful, 
                False: connection failed)
        """
        try:
            # initialize Serializer, which initializes the ClientNetworkHandler
            self.__serializer = Serializer(self, network_config)
        except ConnectionRefusedError:
            # catch error from ClientNetworkHandler if connection to server failed
            self.__serializer = None
            return False
        self.__serializer.serialize(ConnectionRequestMessage())
        return True

    def stop(self):
        """Stops the program"""

        # delegate closing to the serialization layer
        if self.__serializer is not None:
            self.__serializer.close()
        if self.__window is not None:
            self.window.stop()


    def update(self, message: SerializableMessageType):
        """Updates the game with the informations of a given message.

        Args:
            message (SerializableMessageType): Received message
        """
        if isinstance(message, ConnectionConfirmationMessage):
            self.__connect(message.connection_status, message.player_id)
            self.wait_for_other_players()
        elif isinstance(message, PlayerCountMessage):
            self.__player_count = message.player_count
            if self.__on_waiting_screeen:
                self.wait_for_other_players()
        elif isinstance(message, GameStartMessage):
            self.__game_is_running = True
            self.__on_waiting_screeen = False
            if self.__player_id in message.player_ids:
                self.__create_game_state(message.player_ids)
                self.__in_game = True
                self.__sound_player.play_game_music()
                self.__window.update_scene_information(GameSceneInformation(self.__game_state))
                if self.__ping_thread is not None:
                    self.__ping_thread.join()
                    self.__ping_thread = None
            else :
                self.__in_game = False
        elif isinstance(message, GameStateMessage):
            if self.__in_game and self.__game_is_running:
                self.__update_game_state(message.ball_positions, message.player_angles, message.player_lives)
                self.__window.update_scene_information(GameSceneInformation(self.__game_state))
        elif isinstance(message, GameStopMessage):
            self.__game_is_running = False
            if self.__in_game:
                self.__in_game = False
                winner_indices = self.__get_winner_indices(message.winner_ids)
                self.__sound_player.play_title_music()
                self.__window.update_scene_information(StopSceneInformation(message.winner_ids, winner_indices, message.winner_lives))
        
        elif isinstance(message, IsAliveMessage):
            self.__ping_counter -= 1

    def __connect(self, connection_status: ConnectionStatus, player_id: int):
        """Client has connected to the server.
        
        Args:
            connection_status (ConnectionStatus): Status received from the server
            player_id (int): Id of the client
        """
        if connection_status == ConnectionStatus.FAILED:
            self.__connected = False
        else:
            self.__connected = True
            self.__in_game = False
            self.__player_id = player_id
            if connection_status == ConnectionStatus.SUCCESSFUL_WAITING:
                self.__game_is_running = False
            elif connection_status == ConnectionStatus.SUCCESSFUL_RUNNING:
                self.__game_is_running = True


    def __create_game_state(self, player_ids: List[int]):
        """Creates the game_state object.
        
        Args:
            player_ids (List[int]): Ids of all players in the game.
        """
        self.__game_state = GameState(len(player_ids))
        for i in range(len(player_ids)):
            player_id = player_ids[i]
            self.__game_state.add_player(player_id)
            if player_id == self.__player_id:
                self.__player_index = i
        
    def __update_game_state(self, ball_positions: List[Tuple[float, float]], player_angles: List[float], player_lives: List[int]):
        """Updates the game state.

        Args:
            ball_positions (List[Tuple[float, float]]): Positions of balls as (x,y) coordinate tuples [0., 65536.)
            player_angles (List[float]): Angles of players' platforms [0., 1.]
            player_lives (List[int]): Players' number of remaining lives [0, 255]
        """
        self.__update_balls(ball_positions)
        self.__update_players(player_angles, player_lives)


    def __update_balls(self, ball_positions: List[Tuple[float, float]]):
        """Updates the balls of the game state and adds or deletes ball if necessary.

        Args:
            ball_positions (List[Tuple[float, float]]): Positions of balls as (x,y) coordinate tuples [0., 65536.)
        """
        number_of_current_balls = len(self.__game_state.balls)
        number_of_new_balls = len(ball_positions)

        number_of_balls_to_update = min(number_of_current_balls, number_of_new_balls)
        number_of_balls_to_create = max(0, number_of_new_balls - number_of_balls_to_update)
        number_of_balls_to_delete = max(0, number_of_current_balls - number_of_new_balls)

        index = 0
        for i in range(number_of_balls_to_update):
            self.__game_state.balls[index].position = [ball_positions[index][0], ball_positions[index][1]]
            index += 1
        for i in range(number_of_balls_to_create):
            new_ball = Ball([ball_positions[index][0], ball_positions[index][1]], [1, 0])
            self.__game_state.balls.append(new_ball)
        if number_of_balls_to_delete > 0:
            del self.__game_state.balls[number_of_new_balls:]


    def __update_players(self, player_message_angles: List[float], player_lives: List[int]):
        """Updates the players of the  game state.

        Args:
            player_message_angles (List[float]): Message angles of players' platforms [0., 1.]
            player_lives (List[int]): Players' number of remaining lives [0, 255]
        """
        for index in range(len(self.__game_state.players)):
            player = self.__game_state.players[index]
            if player.player_id != self.__player_id: #The client already knows its angle
                new_angle = math_util.get_angle_from_message_angle(self.__game_state.number_of_players, index, player_message_angles[index])
                player.platform.angle = new_angle
            player.remaining_lives = player_lives[index]

    def __get_winner_indices(self, winner_ids: List[int]) -> List[int]:
        """Returns indices of winners, given their identifiers.

        Args:
            winner_ids (List[int]): Winners' identifiers

        Returns:
            List[int]: Winners' indices
        """
        
        return [idx for idx, player in enumerate(self.__game_state.players) if player.player_id in winner_ids]
    
    def _ping(self) -> None:
        """Ping function to send message to server and check its health. 
        Returns to start scene, if server connection is lost.
        Pinging stops when game starts or server is unreachable.

        This method should run in another thread.
        """
        while not self.__in_game:
            self.__serializer.serialize(IsAliveMessage())
            self.__ping_counter += 1
            if self.__ping_counter > 1:
                self.__connected = False
                self.__window.update_scene_information(StartSceneInformation(error_label_text="Server connection lost"))
                self.__serializer.close(3)
                self.__serializer = None
                return
            time.sleep(self.PING_INTERVAL)
        return 
