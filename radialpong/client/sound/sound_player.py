#!/usr/bin/env python3

"""Script implements the sound player."""

import pyglet

class SoundPlayer:
    """Plays sounds and music using the pyglet library.
    
     Attributes:
        __resources (pyglet.resource.Loader): Sound resource loader
        __player (pyglet.media.Player): Sound player
     """

    def __init__(self):
        self.__resources = None
        self.__player = None
        
    
    def start(self) -> None:
        """Starts the sound player. This method has to be called after initialisation."""
        pyglet.options['audio'] = ('openal', 'pulse', 'directsound', 'silent')
        self.__player = pyglet.media.Player()
        self.__player.loop = True
        self.__resources = pyglet.resource.Loader(['client/sound/music'])
        self.__title_music = self.__resources.media('night_shade.wav')
        self.__game_music = self.__resources.media('eight_bit_adventure.wav')


    def play_title_music(self) -> None:
        """Plays the title music."""
        self.__player.next_source()
        self.__player.queue(self.__title_music)
        self.__player.play()


    def play_game_music(self) -> None:
        """Plays the title music."""
        self.__player.next_source()
        self.__player.queue(self.__game_music)
        self.__player.play()
