# Coding guidelines
## Naming conventions
- use names that can be understood without any documentation (e.g. `server_ip` instead of `s_ip`)
- in general all names of variables, functions, methods, ... should be in lower case  with `_` as seperator (e.g. `function_add`)
- class names should always be in upper case without any seperator (e.g. `ExampleClass`)

## General conventions for scripts
- scripts should contain `#!/usr/bin/env python3` as first line
- afterwards an empty line and in the next line a small script definition (docstring, e.g. `"""Script implements camera stream"""`)should be added
- finally the imports follow, seperated from the docstring by another empty line
- all scripts were designed with Python 3.9.2 and used packages are locatet in [requirements](./requirements.txt)

## Definition of classes
- after the definition `class ClassName(object):` the next line should contain a simple description what the class is used for
```
class TCPServer(object):
    """Represents the TCP server and can be started with ..."""
```
- every method should include a docstring, which is a short description of its functionality, the input parameter and the return value(-s) (refer to [Google Style Python Docstrings](https://sphinxcontrib-napoleon.readthedocs.io/en/latest/example_google.html) for more details)
```
    def start_server(ip_address, port, **kwargs):
        """Start the tcp server on the given port.
        
        Args:
            ip_address(string): ip address to start TCP server on
            port(int): port number the TCP server uses
        
        Returns:
            bool: status on start
        
        Raises:
            ConnectionError: Connection refused 
            ...here one should note all exceptions that are raised by this function; this section is optional
        """
```
## Add new message types
New message types can be added as children of ```SerializableMessageType``` class. Each child must provide an implementation for serialization and deserialization. For examples refer to [this folder](./shared/serialization) and look into files with a ```_message``` suffix. 

## Add new scene types
New scene types are added like message types and must be children of ```Scene``` class. The scene file can be found [here](./client/gui/scene.py). Add your new scene type to the ```SceneType``` enum and to the [scene_factory](./client/gui/scene_factory.py) method. Used pictures or other graphics should be placed into the [img folder](./client/gui/img). 
To minimize read accesses load the graphics as class variables and use them. Examples can be found in existing scene files, identified by the ```_scene``` suffix. 

# Milestones 
- Refer to scrum increments

# Branching guidelines
- the ```master``` branch is meant to be updated after each sprint
    - Bugfixes are allowed and performed on a short-lived branch, starting with ```hotfix_<issue_numer>``` (if necessary a rebase of the develop branch may be performed)
- One ```development``` branch per sprint (at the end of the sprint the branch is merged into the master)
- Feature branches arising from the development branch
    - Starting with ```feature_<speaking_name>```
    - branching from feature branches is allowed
