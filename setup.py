from setuptools import setup
from radialpong.shared import metadata

setup(name=metadata.NAME,
      version=metadata.VERSION,
      description='Enjoy the 1972 classic in new splendor - Radial PONG literally marks a turning point and will raise your Pong experience to a completely new level!',
      url='https://gitlab.com/Dennis220999/radial-pong',
      author=metadata.AUTHORS,
      license='MIT',
      packages=['radialpong'],
      python_requires='>=3.9',
      install_requires=[
          'numpy >=1.20.1',
          'pyglet >=1.5.15, <2.0',
          'glooey >=0.3.5, <1.0',
          'tabulate >= 0.8.9, <1.0',
          'clipboard >=0.0.4, <1.0',
          'autoprop <= 1.0.2'
      ],
      zip_safe=False)