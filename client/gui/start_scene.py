#!/usr/bin/env python3

from client.network.network_handler import NetworkConfiguration
from glooey.containers import HBox, VBox, Stack
from glooey.images import Image
from client.gui.gui_elements import ButtonLabel, CustomButton, CustomEdit, EditLabel, ErrorLabel
import ipaddress
import pyglet

from client.gui.scene_information import SceneInformation, SceneType, WaitingSceneInformation
from client.gui.scene import Scene

class StartScene(Scene):
    """Implements StartScene"""
    def __init__(self, scene_information: SceneInformation, window: 'Window', game: 'Game') -> None:
        """Initializes StartScene object.
        
        Args:
            scene_information(SceneInformation): provide a SceneInformation
            window (Window):
            game (Game):
            
        Raises:
            TypeError: StartScene object can only be initialized with scene information of type START_SCENE"""
        
        if scene_information.scene_type != SceneType.START_SCENE:
            raise TypeError("Provided scene information must be of type SceneType.START_SCENE but is of type %s" % scene_information.scene_type)
        super().__init__(scene_information, window, game)

        
    def create(self) -> None:
        super().create()
        # create stack that contains all widgets
        self.__stack = Stack()
        self._gui.add(self.__stack)

        # add background image
        self.__background = Image(image=pyglet.resource.image('background.png'))
        self.__stack.add(self.__background)

        # create VBox for the main column
        self.__column = VBox()
        self.__column.set_alignment('top')
        self.__stack.add(self.__column)

        # place banner image
        self.__banner = Image(image=pyglet.resource.image('banner.png'))
        self.__banner.top_padding = 50
        self.__banner.bottom_padding = 100
        self.__column.add(self.__banner)

        # place text labels
        self.__input = HBox()
        self.__column.pack(self.__input)
        self.__ip_label = CustomEdit("IP address",CustomEdit.LARGE)
        self.__input.pack(self.__ip_label)
        self.__input.pack(ButtonLabel("::"))
        self.__port_label = CustomEdit("Port", CustomEdit.SMALL)
        self.__input.pack(self.__port_label)
        self.__input.set_alignment('center')

        # place connect button and label
        self.__connect = CustomButton("Connect")
        self.__connect.set_alignment('center')
        self.__connect.set_padding(right=self._window_scale[0] // 100, bottom=self._window_scale[1] // 15)
        self.__connect.push_handlers(on_click=self.connect)
        self.__column.pack(self.__connect)
        self.__error_label = ErrorLabel("")
        self.__column.pack(self.__error_label)

        # place quit button
        self.__quit = CustomButton("Quit")
        self.__quit.set_alignment('bottom left')
        self.__quit.set_padding(left=self._window_scale[0] // 100, bottom=self._window_scale[1] // 15)
        self.__quit.push_handlers(on_click=self._quit_game)
        self.__stack.add(self.__quit)


    def _modify(self):
        pass

    def _quit_game(self, widget : 'CustomButton') -> None:
        """Method run by \"Quit\" button.
        
        Args:
            widget(CustomButton): button widget to act as a handler function
        """
        self._game.stop()

    
    def __check_ip(self, ip : str) -> bool:
        """Private method to validate IP provided from user.
        
        Args:
            ip(String): IP provided in String format, optained from e.g. Glooey Label
            
        Returns:
            Boolean: Is IP valid"""
        try:
            ipaddress.IPv4Address(ip)
        except ipaddress.AddressValueError:
            return False
        return True

    def connect(self, widget : 'CustomButton') -> None:
        """Method run by \"Connect"\ button.
        Validation of input is performed here. If invalid values are entered,
        the text of the wrong input field get's removed.

        If successful, this method changes window scene to waiting scene.

        Args:
            widget(CustomButton): button widget to act as a handler function
        """
        self.__error_label.set_text("")

        ip_str = str(self.__ip_label.get_text())

        # check port number
        try:
            port = int(self.__port_label.get_text())
            if 0 > port > 65535:
                raise ValueError
        except ValueError:
            self.__error_label.set_text("Wrong port format")
            return

        # check ip
        if self.__check_ip(ip_str) is True:
            if not self._game.start_connection(NetworkConfiguration(ip_str,port)):
                self.__error_label.set_text("Server not reachable")
            return
        else:
            self.__error_label.set_text("Wrong ip format")
            return
        