#!/usr/bin/env python3

"""Script implements the window class."""

from client.gui.scene import Scene
import pyglet
from pyglet.gl.gl import GL_LINE_SMOOTH, GL_POINT_SMOOTH, GL_SMOOTH # is not used directly, but set by opengl
import queue
 
from client.gui.scene_information import SceneInformation, StartSceneInformation
from client.gui.event_loop import EventLoop
from client.gui.scene_factory import create_scene


class Window(pyglet.window.Window):
    """Represents the window containing the GUI of the game.

    Args:
        __game (Game): Main game object
        __scene (Scene): The scene that is currently displayed in the window
    """
    
    __WIDTH = 1280
    __HEIGHT = 720
    __REFRESH_RATE = 1/60

    def __init__(self, game: 'Game'):
        super().__init__(width=self.__WIDTH, height=self.__HEIGHT)
        self.__game = game
        self.__scene_information_queue = queue.Queue()
        self.__scene_information_queue.put(StartSceneInformation())
        self.__event_loop = None
        self.__scene = None

        # opengl settings
        pyglet.gl.glClearColor(1,1,1,1) # set background color to white
        pyglet.gl.glEnable(pyglet.gl.GL_LINE_SMOOTH) # draw smooth lines
        
        # pyglet resource settings
        pyglet.resource.path = ['client/gui/img']
        pyglet.resource.reindex()

        # window settings
        self.set_caption("Pong")
        self.set_icon(pyglet.resource.image('window_icon.png'))


    def start(self):
        """Starts the window."""
        self.__event_loop = EventLoop(self)
        pyglet.app.event_loop = self.__event_loop
        self.__clock = pyglet.clock
        self.__clock.schedule_interval(self.__process_new_scene_information, self.__REFRESH_RATE)
        pyglet.app.run()

    def __process_new_scene_information(self, dt : float) -> None:
        if not self.__scene_information_queue.empty():
            scene_information = self.__scene_information_queue.get()
            if self.__scene is not None and self.__scene.scene_type == scene_information.scene_type:
                # update scene object
                self.__scene.update(scene_information)
            else:
                # create new scene if type changed
                self.__scene = create_scene(scene_information, self, self.__game)
                self.__scene.create()

    def stop(self):
        """Stops the window."""
        pyglet.app.exit()


    def on_draw(self):
        self.clear()
        if self.__scene is not None:
            self.__scene.draw()


    def update_scene_information(self, scene_information: 'SceneInformation'):
        """Updates the display scene.
        
        Args:
            scene_information (SceneInformation): New scene information
        """
        self.__scene_information_queue.put(scene_information)

    def on_close(self):
        self.__game.stop()
