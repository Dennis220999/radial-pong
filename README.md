# Radial Pong
![Logo](https://gitlab.com/Dennis220999/radial-pong/-/wikis/uploads/e78a1e14e63a033980b66f4258e173f0/SPOILER_Logo.png)

&nbsp;

## Quicklinks

- [**Documentation**](https://dennis220999.gitlab.io/-/radial-pong/-/jobs/1184309762/artifacts/public/radialpong/index.html)
- [**Download (ZIP)**](https://gitlab.com/Dennis220999/radial-pong/-/archive/v1.0/radial-pong-v1.0.zip)
- [**Download (TAR.GZ)**](https://gitlab.com/Dennis220999/radial-pong/-/archive/v1.0/radial-pong-v1.0.tar.gz)

&nbsp;

## Project description

*Enjoy the 1972 classic in new splendor — Radial PONG literally marks a turning point and will raise your Pong experience to a completely new level!*

Radial PONG is a game originated from a study project that picks up the idea of the original 1972 Pong game by Atari, but sets up the game board as a circle. In consequence, more than two players can play against each other. It is fully developed in Python and utilizes the library [pyglet](http://pyglet.org/) to draw graphics, internally using OpenGL.

The game is designed in a server-client-architecture, i.e. one server is needed to host the game logic and several clients can connect to it, acting as players. Here, a user can theoretically start several client instances at once, independent of simultaneously hosting the game as a server or not. Communication is realized using a custom message protocol, which is already based on TCP/IP, but could independently detect corrupt messages due to CRC (Cyclic Redundancy Check). Furthermore, it automatically corrects wrong message orders and is aware of messages that got lost.

&nbsp;

## Screenshots

| | |
|:-:|:-:|
|![Pong_1](https://gitlab.com/Dennis220999/radial-pong/-/wikis//uploads/3e2fb7fde373a72a87fd11143924940a/screenshot_1.png)**Start scene**|![Pong_2](https://gitlab.com/Dennis220999/radial-pong/-/wikis//uploads/f01e9917352868ce3ecc2ccd458e4935/screenshot_2.png)**Waiting scene**|
|![Pong_3](https://gitlab.com/Dennis220999/radial-pong/-/wikis//uploads/449d8a441d46a154e6dea4a403ddd202/screenshot_3.png)**Game scene**|![Pong_4](https://gitlab.com/Dennis220999/radial-pong/-/wikis//uploads/32ef38669951011d04c9dcf08b9f15f3/screenshot_4.png)**Stop scene**|

&nbsp;

## Setup

### Prerequisites:

- [Python](https://www.python.org/) >= 3.9

### Installation:

- Create and activate a new virtual environment (e.g. using the [*venv*](https://docs.python.org/3/tutorial/venv.html) module).
- Unpack the downloaded project files and run `python setup.py install`.

&nbsp;

## Usage

### Server:

- Run `python radialpong/main_server.py` inside the virtual environment.
- Share the IP address and the port being printed in the console with all willing players.
- Press `CTRL + C` to stop the server.

### Client:

- Run `python radialpong/main_client.py` inside the virtual environment.
- Insert the IP address of the server as well as the used port on the start screen.
- Connect to the server and wait for other participating players.
- Start the game (only one connected player has to request game start).
- Move the mouse horizontally to move your platform and reflect the balls.
- After the game is finished, you can return to the waiting screen in order to start another game.
- Close the window to stop the client application.
